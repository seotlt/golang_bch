package protocol

import (
	"fmt"
	"testing"
)

func TestBlockSaving(t *testing.T) {
	fmt.Println(GetLatestBlock())
}

func TestGetAllBlocks(t *testing.T) {
	fmt.Println(GetAllBlocks())
}

func TestBlockParsing(t *testing.T) {
	block := GetGenesisBlock()
	fmt.Println(block)
}
