package protocol

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strconv"

	"gitlab.com/seotlt/golang_bch/protocol/wallet"

	"gitlab.com/seotlt/golang_bch/db"
)

// Coin is the structure of transaction of type "Coin"
type Coin struct {
	Transaction `mapstructure:",squash"`
}

// Input contains data about available amount of coins
type Input struct {
	TxHash string `json:"txhash" mapstructure:"txhash"`
	Index  int    `json:"index" mapstructure:"index"`
	Amount int    `json:"amount" mapstructure:"amount"`
}

// Output contains data to whom and how much sended
type Output struct {
	Address string `json:"address" mapstructure:"address"`
	Amount  int    `json:"amount" mapstructure:"amount"`
}

const (
	// CommissionPercent represents percent of commission for transactions
	CommissionPercent = 0.01
)

var (
	upInputsDB = db.GetInstance("upInputs")
)

// SaveTX needs to save tx in DB
func (coin Coin) SaveTX() error {
	if err := coin.Transaction.SaveTX(); err != nil {
		return err
	}

	return nil
}

// IsValid checks transaction
func (coin Coin) IsValid() error {
	if err := coin.Transaction.IsValid(); err != nil {
		return err
	}

	if err := isInputsMore(coin.Inputs, coin.Outputs); err != nil {
		return err
	}

	if err := coin.checkCommission(); err != nil {
		return err
	}

	if err := checkDoubleWaste(coin.From, coin.Inputs, strconv.Itoa(int(coin.TxType))); err != nil {
		return err
	}

	return nil
}

// CheckMempool checks availability to pass transaction in Mempool
func (coin Coin) CheckMempool(mempool map[string]TransactionInt) error {
	if err := coin.Transaction.CheckMempool(mempool); err != nil {
		return err
	}

	return nil
}

func (coin Coin) checkCommission() error {
	outputs := coin.Outputs
	sendAmount := outputs[0].Amount
	computedCommission := computeCommission(sendAmount)
	if computedCommission != coin.Commission {
		err := fmt.Sprintf("Commission in transaction is wrong. True commission: %d", computedCommission)
		return errors.New(err)
	}

	inputs := coin.Inputs
	var inputsSum int
	for _, input := range inputs {
		inputsSum += input.Amount
	}

	if len(outputs) > 1 {
		backAmount := outputs[1].Amount
		computedBack := inputsSum - sendAmount - computedCommission
		if computedBack != backAmount {
			err := fmt.Sprintf("Invalid remainder output amount. Must be %d", computedBack)
			return errors.New(err)
		}
	} else {
		isPayed := inputsSum - sendAmount
		if isPayed != computedCommission {
			return errors.New("Commission hasn't been payed")
		}
	}

	return nil
}

func isInputsMore(inputs []Input, outputs []Output) error {
	var inputsSum, outputsSum int
	for _, input := range inputs {
		inputsSum += input.Amount
	}

	for _, output := range outputs {
		outputsSum += output.Amount
	}

	if inputsSum < outputsSum {
		return errors.New("inputs less than outputs")
	}

	return nil
}

// GenCoin generates new Coin transaction
func GenCoin(from wallet.Wallet, to string, txType int8, optParams map[string]string) (*Coin, error) {
	if _, has := optParams["amount"]; !has {
		return nil, errors.New("Amount argument is necessary for coin tx")
	}

	amount, err := strconv.Atoi(optParams["amount"])
	if err != nil {
		return nil, err
	}

	var inputs []Input
	var remainder int
	var commission int
	if optParams["fee"] != "true" {
		commission = computeCommission(amount)
		amountWithComm := amount + commission

		inputs, remainder, err = FormInputs(txType, "", from.Address, amountWithComm)
		if err != nil {
			return nil, err
		}
	}
	outputs := FormOutputs(from.Address, to, amount, remainder)

	return NewCoin("", from.Address, txType, commission, inputs, outputs, from.PublicKey, 0, ""), nil
}

func computeCommission(amount int) int {
	commission := int((float64(amount) * CommissionPercent))
	if commission == 0 {
		commission = 1
	}

	return commission
}

func checkDoubleWaste(from string, inputs []Input, txType string) error {
	for _, input := range inputs {
		key := fmt.Sprintf("%s%s_%s_%d", from, txType, input.TxHash, input.Index)
		if _, err := upInputsDB.Get([]byte(key)); err != nil {
			return errors.New("Attempt to double waste")
		}

		_, err := txDB.Get([]byte(input.TxHash))
		if err != nil {
			return errors.New("Attempt to double waste")
		}
	}

	return nil
}

// NewCoin is a constructor for Coin
func NewCoin(
	hash string,
	from string,
	txType int8,
	commission int,
	inputs []Input,
	outputs []Output,
	publicKey string,
	timestamp int64,
	signature string,
) *Coin {
	tx := new(Coin)
	tx.initBasicAttr(hash, from, txType, commission, inputs, outputs, publicKey, timestamp, signature)

	return tx
}

// GetBalance returns balance of specified address
func GetBalance(txType int8, tkName string, address string) int {
	balance := 0
	unspentInputs := getUnspentInputs(txType, tkName, address)

	for _, input := range unspentInputs {
		balance += input.Amount
	}

	return balance
}

// FormInputs collects and forms necessary inputs for coin tx
func FormInputs(txType int8, tkName string, from string, amount int) ([]Input, int, error) {
	var inputs []Input
	sum := 0
	unspentInputs := getUnspentInputs(txType, tkName, from)
	for _, input := range unspentInputs {
		sum += input.Amount
		inputs = append(inputs, input)

		if sum >= amount {
			break
		}
	}
	remainder := sum - amount
	if remainder < 0 {
		return nil, 0, errors.New("You don't have enough money")
	}

	return inputs, remainder, nil
}

// FormOutputs forms necessary outputs for coin tx
func FormOutputs(from string, to string, amount int, remainder int) []Output {
	var outputs []Output

	toOutput := Output{
		Address: to,
		Amount:  amount,
	}
	outputs = append(outputs, toOutput)

	if remainder > 0 {
		backOutput := Output{
			Address: from,
			Amount:  remainder,
		}
		outputs = append(outputs, backOutput)
	}

	return outputs
}

func getUnspentInputs(typeOfTx int8, tkName string, address string) []Input {
	var prefix string
	if tkName != "" {
		prefix = fmt.Sprintf("%s%s", address, tkName)
	} else {
		prefix = fmt.Sprintf("%s%d", address, typeOfTx)
	}
	iter := upInputsDB.NewIteratorWithPrefix([]byte(prefix))

	var upInputs []Input
	for iter.Next() {
		var upInput Input
		if err := json.Unmarshal(iter.Value(), &upInput); err != nil {
			log.Println(err)
		}

		upInputs = append(upInputs, upInput)
	}

	return upInputs
}
