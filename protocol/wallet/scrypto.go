package wallet

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/ecdsa"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/ethereum/go-ethereum/crypto"
	"gitlab.com/seotlt/golang_bch/protocol/helper"
	"golang.org/x/crypto/scrypt"
)

var (
	currentWallet Wallet

	ksPath = "./.keystores"
)


func init() {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	ksPath = exPath+"/.keystores"
}

// Wallet structure for wallet info
type Wallet struct {
	PrivateKey string
	PublicKey  string
	Address    string
}

type keystore struct {
	CipherText string    `json:"ciphertext"`
	IV         string    `json:"iv"`
	KdfParams  kdfparams `json:"kdfparams"`
	MAC        string    `json:"mac"`
}

type kdfparams struct {
	N     int    `json:"n"`
	R     int    `json:"r"`
	P     int    `json:"p"`
	Salt  string `json:"salt"`
	DKLen int    `json:"dklen"`
}

// CreateWallet generate new wallet
func CreateWallet(password []byte, privKey string) (*Wallet, error) {
	wl, err := GenWallet(privKey)
	if err != nil {
		return nil, err
	}

	if err := createKeystore(wl.Address, []byte(wl.PrivateKey), password); err != nil {
		return nil, err
	}
	currentWallet = *wl

	return wl, nil
}

// GenWallet generates new or import wallet
func GenWallet(privKey string) (*Wallet, error) {
	var key *ecdsa.PrivateKey
	var err error
	if privKey != "" {
		key, err = crypto.HexToECDSA(privKey)
	} else {
		key, err = crypto.GenerateKey()
	}
	if err != nil {
		return nil, err
	}

	address := strings.ToLower(crypto.PubkeyToAddress(key.PublicKey).Hex())
	publicKey := (hex.EncodeToString(crypto.FromECDSAPub(&key.PublicKey))[2:])

	privateKey := hex.EncodeToString(key.D.Bytes())

	wl := Wallet{PrivateKey: privateKey, PublicKey: publicKey, Address: address}

	return &wl, nil
}

// UnlockWallet executes private key from keystore file
// by password and sets wallet as current
func UnlockWallet(ksFilename string, password []byte) error {
	pk, err := readKeystore(ksFilename, password)
	if err != nil {
		return err
	}

	wlAddress := "0x" + ksFilename[:len(ksFilename)-5]
	wl := Wallet{
		Address:    wlAddress,
		PrivateKey: pk,
		PublicKey:  helper.FromPrivateToPublic(pk),
	}

	currentWallet = wl

	return nil
}

// DeleteWallet deletes keystore file from the file system
func DeleteWallet(fname string) error {
	ksFilename := ksPath + "/" + fname
	if err := os.Remove(ksFilename); err != nil {
		return err
	}

	return nil
}

// CheckAddress checks is address correct or not
func CheckAddress(address string) error {
	if !(address[0] == '0' && address[1] == 'x') {
		return errors.New("Address must have '0x' at the beginning")
	}

	if len(address) != 42 {
		return errors.New("Invalid length of address. Have to be 42")
	}

	return nil
}

// GetCurrentWallet returns current authorized wallet
func GetCurrentWallet() (Wallet, error) {
	if currentWallet.PrivateKey == "" {
		return Wallet{}, errors.New("current wallet does not set")
	}
	return currentWallet, nil
}

// GetAllWalletFiles returns all keystore files
func GetAllWalletFiles() ([]os.FileInfo, error) {
	// does dir ".keystores" exist?
	if _, err := os.Stat(ksPath); os.IsNotExist(err) {
		if err := os.Mkdir(ksPath, os.ModePerm); err != nil {
			return nil, errors.New("Failed to make dir: " + err.Error())
		}
	}

	keystores, err := ioutil.ReadDir(ksPath)
	if err != nil {
		return nil, errors.New("Failed to read dir: " + err.Error())
	}

	return keystores, nil
}

func createKeystore(address string, pk, password []byte) error {
	const (
		n     = 32768
		r     = 8
		p     = 1
		dklen = 32
	)

	salt := helper.GenSalt()
	dk, err := scrypt.Key(password, salt, n, r, p, dklen)
	if err != nil {
		return errors.New("Failed to create decryption key: " + err.Error())
	}

	block, err := aes.NewCipher(dk)
	if err != nil {
		return errors.New("Failed to create new cipher: " + err.Error())
	}

	cipherPK := make([]byte, len(pk))
	iv := make([]byte, aes.BlockSize)
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return errors.New("Failed to generate iv: " + err.Error())
	}

	stream := cipher.NewCTR(block, iv)
	stream.XORKeyStream(cipherPK[:], pk)

	macHash := hmac.New(sha256.New, append(dk[:], cipherPK[:]...))
	mac := hex.EncodeToString(macHash.Sum(nil))

	ks := keystore{
		CipherText: hex.EncodeToString(cipherPK),
		IV:         hex.EncodeToString(iv),
		KdfParams: kdfparams{
			N:     n,
			R:     r,
			P:     p,
			Salt:  hex.EncodeToString(salt),
			DKLen: dklen,
		},
		MAC: mac,
	}

	ksJSON, err := json.Marshal(ks)
	if err != nil {
		return errors.New("Failed to convert keystore in json: " + err.Error())
	}

	// does dir ".keystores" exist?
	if _, err := os.Stat(ksPath); os.IsNotExist(err) {
		if err := os.Mkdir(ksPath, os.ModePerm); err != nil {
			return errors.New("Failed to make dir: " + err.Error())
		}
	}

	ksFilename := ksPath + "/" + address[2:] + ".json"
	ksFile, err := os.Create(ksFilename)
	if err != nil {
		return errors.New("Failed to create keystore file: " + err.Error())
	}

	if _, err := ksFile.WriteString(string(ksJSON)); err != nil {
		return errors.New("Failed to write in keystore file: " + err.Error())
	}

	ksFile.Close()

	return nil
}

func readKeystore(ksFilename string, password []byte) (string, error) {
	ksFilePath := ksPath + "/" + ksFilename
	if _, err := os.Stat(ksFilePath); os.IsNotExist(err) {
		return "", errors.New("Keystore does not exist")
	}

	ksData, err := ioutil.ReadFile(ksFilePath)
	if err != nil {
		return "", errors.New("Failed to read keystore: " + err.Error())
	}

	var ks keystore
	if err := json.Unmarshal(ksData, &ks); err != nil {
		return "", errors.New("Failed to unmarshal keystore: " + err.Error())
	}

	kdf := ks.KdfParams
	salt, err := hex.DecodeString(kdf.Salt)
	if err != nil {
		return "", errors.New("Failed to decode salt: " + err.Error())
	}

	dk, err := scrypt.Key(
		password,
		salt,
		kdf.N,
		kdf.R,
		kdf.P,
		kdf.DKLen,
	)
	if err != nil {
		return "", errors.New("Failed to create decryption key: " + err.Error())
	}

	cipherPK, err := hex.DecodeString(ks.CipherText)
	if err != nil {
		return "", errors.New("Failed to decode cipher pk: " + err.Error())
	}

	expectedMAC, err := hex.DecodeString(ks.MAC)
	if err != nil {
		return "", errors.New("Failed to decode mac: " + err.Error())
	}

	givenMAC := hmac.New(sha256.New, append(dk[:], cipherPK[:]...))
	givenMACByte := givenMAC.Sum(nil)
	if !hmac.Equal(expectedMAC, givenMACByte) {
		return "", errors.New("Entered password is not valid")
	}

	block, err := aes.NewCipher(dk)
	if err != nil {
		return "", errors.New("Failed to create new cipher: " + err.Error())
	}

	iv, err := hex.DecodeString(ks.IV)
	if err != nil {
		return "", errors.New("Failed to decode iv: " + err.Error())
	}

	pk := make([]byte, 64)
	stream := cipher.NewCTR(block, iv)
	stream.XORKeyStream(pk, cipherPK)

	return string(pk), nil
}
