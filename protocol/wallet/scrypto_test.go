package wallet

import (
	"fmt"
	"testing"
)

func TestCreateWallet(t *testing.T) {
	CreateWallet([]byte("lolkek"), "")
}

func TestKeystoreCreating(t *testing.T) {
	if err := createKeystore(
		"0xe89a52f807f1c13dd8643d7eba14a9331be44b7d",
		[]byte("1b9fc76d5f68c0c9eb4cbcc5c35d41a89083d5b869fbf1300733365eea821be5"),
		[]byte("lolkek"),
	); err != nil {
		panic(err)
	}
}

func TestKeystoreReading(t *testing.T) {
	err := UnlockWallet(
		"e89a52f807f1c13dd8643d7eba14a9331be44b7d.json",
		[]byte("lolkek"),
	)
	if err != nil {
		panic(err)
	}
}

func TestGettingAllWallets(t *testing.T) {
	keystores, err := GetAllWalletFiles()
	if err != nil {
		panic(err)
	}

	for _, keystore := range keystores {
		fmt.Println(keystore.Name())
	}
}
