package protocol

import (
	"io/ioutil"
	"log"
	"os"
	"testing"
)

func TestGettingFileContent(t *testing.T) {
	path := "../testlogfile"
	if _, err := os.Stat(path); os.IsNotExist(err) {
		log.Fatalln("File does not exist!")
	}

	fileData, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalln("Failed to read file:", err)
	}

	log.Println(string(fileData))
}
