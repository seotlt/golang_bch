package protocol

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/syndtr/goleveldb/leveldb/iterator"

	"github.com/cbergoon/merkletree"
	"github.com/ethereum/go-ethereum/crypto"
	"gitlab.com/seotlt/golang_bch/db"
	"gitlab.com/seotlt/golang_bch/protocol/helper"
	"gitlab.com/seotlt/golang_bch/protocol/wallet"
)

const (
	initialBaseTarget = 153722867
)

var (
	blockDB       = db.GetInstance("block")
	blockHashesDB = db.GetInstance("blockHashes")
	forCumDiff    big.Int //2^64

	pathToGenesis = "./genesis.json"
	latestBlock  Block
	genesisBlock Block

	// MaxBaseTarget is the max base target that can be
	MaxBaseTarget = initStateAmount * initialBaseTarget
)

// Block is a struct that represents Block
type Block struct {
	Index          int              `json:"index"`
	Hash           string           `json:"hash"`
	PrevHash       string           `json:"prevHash"`
	Transactions   []TransactionInt `json:"txs"`
	MerkleRoot     string           `json:"merkleroot"`
	Timestamp      int              `json:"timestamp"`
	BaseTarget     int              `json:"baseTarget"`
	GenSignature   string           `json:"genSignature"`
	Signature      string           `json:"signature"`
	CumulativeDiff string           `json:"cumulativeDiff"`
	Generator      string           `json:"generator"`
	PublicKey      string           `json:"publicKey"`
}

// BlockParser needs for parsing Block with Transactions from JSON
type BlockParser struct {
	Index          int           `json:"index"`
	Hash           string        `json:"hash"`
	PrevHash       string        `json:"prevHash"`
	Transactions   []interface{} `json:"txs"`
	MerkleRoot     string        `json:"merkleroot"`
	Timestamp      int           `json:"timestamp"`
	BaseTarget     int           `json:"baseTarget"`
	GenSignature   string        `json:"genSignature"`
	Signature      string        `json:"signature"`
	CumulativeDiff string        `json:"cumulativeDiff"`
	Generator      string        `json:"generator"`
	PublicKey      string        `json:"publicKey"`
}

// UnmarshalJSON - custom unmarshal JSON for Block
func (block *Block) UnmarshalJSON(b []byte) error {
	var blockParser BlockParser
	if err := json.Unmarshal(b, &blockParser); err != nil {
		return err
	}

	var transactions []TransactionInt
	for _, txInt := range blockParser.Transactions {
		txMap := txInt.(map[string]interface{})
		tx := txMapToTX(txMap)

		transactions = append(transactions, tx)
	}

	block.Index = blockParser.Index
	block.Hash = blockParser.Hash
	block.PrevHash = blockParser.PrevHash
	block.Transactions = transactions
	block.MerkleRoot = blockParser.MerkleRoot
	block.Timestamp = blockParser.Timestamp
	block.BaseTarget = blockParser.BaseTarget
	block.GenSignature = blockParser.GenSignature
	block.Signature = blockParser.Signature
	block.CumulativeDiff = blockParser.CumulativeDiff
	block.Generator = blockParser.Generator
	block.PublicKey = blockParser.PublicKey

	return nil
}

func init() {
	forCumDiff.SetString("18446744073709551616", 10)
}

// SaveBlock needs for saving block in DB
func (block Block) SaveBlock() error {
	keyHash := []byte(block.Hash)
	blockJSON, err := json.Marshal(block)
	if err != nil {
		return err
	}

	err = blockDB.Put(keyHash, blockJSON)
	if err != nil {
		log.Println(err)

		return err
	}
	go removeTXsFromMempool(block.Transactions)

	keyIndex := []byte(strconv.Itoa(block.Index))
	err = blockHashesDB.Put(keyIndex, keyHash)
	if err != nil {
		log.Println(err)

		return err
	}

	_, err = blockDB.Get(keyHash)
	if err != nil {
		return err
	}

	return nil
}

// Sign is a method for signing block
func (block *Block) Sign(privateKeyStr string) error {
	privateKey, err := crypto.HexToECDSA(privateKeyStr) //convert to ecdsa.PrivateKey
	if err != nil {
		return err
	}

	orderedTXS := getOrderedTXSHashes(block.Transactions)
	merkleRoot, err := helper.GetMerkleRoot(orderedTXS)
	if err != nil {
		merkleRoot = []byte("0")
	}

	block.MerkleRoot = hex.EncodeToString(merkleRoot)

	toSign := *block
	toSign.Transactions = nil

	blockJSON, err := json.Marshal(toSign)
	if err != nil {
		return err
	}

	msgHash := sha256.Sum256(blockJSON) //hash tx struct

	signature, err := crypto.Sign(msgHash[:], privateKey)
	if err != nil {
		return err
	}

	signatureStr := hex.EncodeToString(signature)
	block.Signature = signatureStr

	block.genBlockHash() //gen block hash after creation of signature

	return nil
}

// VerifySign is a func to verify signature of block
func (block Block) VerifySign() error {
	signature := block.Signature

	block.Signature = ""
	block.Hash = ""
	block.Transactions = nil

	publicKey, err := hex.DecodeString("04" + block.PublicKey) //to bytes arr
	if err != nil {
		return err
	}

	// Check conforming public key and 'from' address
	pubKeyECDSA, err := crypto.UnmarshalPubkey(publicKey)
	if err != nil {
		return err
	}
	addrFromPub := crypto.PubkeyToAddress(*pubKeyECDSA).Hex()

	if strings.ToLower(addrFromPub) != block.Generator {
		return errors.New("Public key in block doesn't conform to 'generator' address")
	}

	blockJSON, err := json.Marshal(block)
	if err != nil {
		return err
	}

	msgHash := sha256.Sum256(blockJSON)
	byteSignature, err := hex.DecodeString(signature)
	if err != nil {
		return err
	}

	// IMPORTANT: remove recovery ID (without last byte)
	signatureNoRecoverID := byteSignature[:len(byteSignature)-1]
	valid := crypto.VerifySignature(publicKey, msgHash[:], signatureNoRecoverID)
	if !valid {
		return errors.New("Invalid signature of block")
	}

	return nil
}

func (block Block) checkCoinbase() error {
	txs := block.Transactions
	if len(txs) == 0 {
		return nil
	}

	coinbase := txs[0].GetStruct()
	if coinbase.TxType != 0 || len(coinbase.Inputs) != 0 {
		return nil
	}

	recipient := coinbase.Outputs[0].Address
	if recipient != block.Generator {
		return errors.New("Invalid fee recipient")
	}

	var feeSum int
	for _, tx := range txs[1:] {
		txStruct := tx.GetStruct()

		feeSum += txStruct.Commission
	}

	cbFee := coinbase.Outputs[0].Amount
	if cbFee != feeSum {
		return errors.New("Invalid fee sum")
	}

	return nil
}

func (block Block) verifyMerkle() error {
	orderedTXS := getOrderedTXSHashes(block.Transactions)
	merkleRoot, err := helper.GetMerkleRoot(orderedTXS)
	if err != nil {
		merkleRoot = []byte("0")
	}

	merkleRootHex := hex.EncodeToString(merkleRoot)
	if merkleRootHex != block.MerkleRoot {
		return errors.New("Invalid merkle root")
	}

	return nil
}

func (block Block) verifyGenerationSignature(latestBlock *Block) error {
	genSig := helper.CalcHash([]byte(latestBlock.GenSignature + block.PublicKey))

	if genSig != block.GenSignature {
		err := fmt.Sprintf(`
		Invalid generation signature! 
		Right gen sig(%s) != New block gen sig(%s)`,
			genSig,
			block.GenSignature,
		)
		return errors.New(err)
	}

	hit, err := strconv.ParseInt(genSig[0:12], 16, 64)
	if err != nil {
		return err
	}
	stateAmount := GetBalance(1, "", block.Generator)
	target := latestBlock.BaseTarget * (block.Timestamp - latestBlock.Timestamp) * stateAmount

	if target <= int(hit) {
		err := fmt.Sprintf(`
		Target less than or equal hit!
		Target(%d) <= Hit(%d)`,
			target,
			hit,
		)
		return errors.New(err)
	}

	return nil
}

func (block *Block) genBlockHash() {
	blockJSON, err := json.Marshal(block)
	if err != nil {
		log.Println(err)
	}

	block.Hash = helper.CalcHash(blockJSON)
}

func getOrderedTXSHashes(txs []TransactionInt) []merkletree.Content {
	var txsHashes []helper.MerkleEl
	var data []merkletree.Content

	for _, tx := range txs {
		el := helper.MerkleEl{Hash: tx.GetHash()}
		txsHashes, data = helper.InsertSort(txsHashes, data, el)
	}

	return data
}

// NewBlock is a constructor for the Block struct
func NewBlock(
	index int,
	hash string,
	prevHash string,
	transactions []TransactionInt,
	timestamp int,
	baseTarget int,
	genSignature string,
	signature string,
	cumulativeDiff string,
	generator string,
	publicKey string,
) *Block {
	block := new(Block)

	block.Index = index
	block.PrevHash = prevHash
	block.Transactions = transactions
	block.Generator = generator
	block.PublicKey = publicKey

	block.BaseTarget = baseTarget
	block.GenSignature = genSignature

	block.CumulativeDiff = cumulativeDiff

	if timestamp == 0 {
		timestamp = int(time.Now().Unix()) //seconds
	}
	block.Timestamp = timestamp

	if signature == "" {
		currWallet, err := wallet.GetCurrentWallet()
		if err != nil {
			log.Println(err)
		} else {
			if err := block.Sign(currWallet.PrivateKey); err != nil {
				log.Println(err)
			}
		}
	} else {
		block.Signature = signature
		// if hash doesn't point, but block
		// have all needed fields, generate it
		if hash == "" {
			block.genBlockHash()
		} else {
			block.Hash = hash
		}
	}

	return block
}

// GenNextBlock is a func for generating new next block
func GenNextBlock(
	transactions []TransactionInt,
	generator string,
	publicKey string,
	baseTarget int,
	genSignature string,
) *Block {
	prevBlock := GetLatestBlock()
	nextIndex := prevBlock.Index + 1

	prevHash := prevBlock.Hash

	prevCumDiff := new(big.Int)
	prevCumDiff, _ = prevCumDiff.SetString(prevBlock.CumulativeDiff, 10)

	bigBaseTarget := big.NewInt(int64(baseTarget))
	var cumulativeDiff big.Int
	cumulativeDiff.
		Div(&forCumDiff, bigBaseTarget).
		Add(&cumulativeDiff, prevCumDiff)

	nextBlock := NewBlock(
		nextIndex,
		"",
		prevHash,
		transactions,
		0,
		baseTarget,
		genSignature,
		"",
		cumulativeDiff.String(),
		generator,
		publicKey,
	)

	return nextBlock
}

// IsValidNextBlock checks is new block valid or not
func IsValidNextBlock(nextBlock *Block, previousBlock *Block) error {
	if previousBlock.Index+1 != nextBlock.Index {
		err := fmt.Sprintf(`
		Next block index is invalid!
		Previous index(%d) + 1 != Next block index(%d)`,
			previousBlock.Index,
			nextBlock.Index,
		)
		return errors.New(err)
	}

	if previousBlock.Hash != nextBlock.PrevHash {
		err := fmt.Sprintf(`
		Previous hash of next block is not equal hash of previous block!
		Previous hash(%s) != Next block previous hash(%s)`,
			previousBlock.Hash,
			nextBlock.PrevHash,
		)
		return errors.New(err)
	}

	curTime := int(time.Now().Unix())
	if nextBlock.Timestamp > curTime+5 {
		err := fmt.Sprintf(`
		Block from the future? Or maybe your current time is invalid
		Next block timestamp(%d) > Your current time(%d)`,
			nextBlock.Timestamp,
			curTime,
		)
		return errors.New(err)
	}

	if nextBlock.Timestamp < previousBlock.Timestamp {
		err := fmt.Sprintf(`
			Next block timestamp less than previous block timestamp!
			Next block timestamp(%d) < Previous block timestamp(%d)`,
			nextBlock.Timestamp,
			previousBlock.Timestamp,
		)
		return errors.New(err)
	}

	if err := nextBlock.checkCoinbase(); err != nil {
		return err
	}

	if err := nextBlock.verifyMerkle(); err != nil {
		return err
	}

	if err := nextBlock.VerifySign(); err != nil {
		return err
	}

	if err := nextBlock.verifyGenerationSignature(previousBlock); err != nil {
		return err
	}

	return nil
}

// DeleteBlock deletes block from block and blockHashes db
func DeleteBlock(hash string, index int) error {
	keyIndex := []byte(strconv.Itoa(index))
	if err := blockHashesDB.Delete(keyIndex); err != nil {
		return err
	}

	if err := blockDB.Delete([]byte(hash)); err != nil {
		return err
	}

	return nil
}

// GetGenesisBlock returns genesis block
func getGenesisBlock() Block {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	pathToGenesis = exPath+"/genesis.json"


	if _, err := os.Stat(pathToGenesis); os.IsNotExist(err) {
		panic("Genesis.json file does not exist")
	}

	genBlockJ, err := ioutil.ReadFile(pathToGenesis)
	if err != nil {
		panic("Failed to read genesis.json: " + err.Error())
	}

	var newGenesisBlock Block
	err = json.Unmarshal(genBlockJ, &newGenesisBlock)
	if err != nil {
		panic("Can't unmarshal genesis.json: " + err.Error())
	}

	return newGenesisBlock
}

// SetLatestBlock sets block as latest
func SetLatestBlock(block Block) {
	latestBlock = block
}

// GetLatestBlock returns latest block
func GetLatestBlock() Block {
	return latestBlock
}

// GetGenesisBlock returns genesis block
func GetGenesisBlock() Block {
	return genesisBlock
}

// GetBlocks returns specified amount of blockchain blocks
func GetBlocks(start int, limit int) ([]Block, error) {
	var blockHashesIter iterator.Iterator
	if start == 0 && limit == 0 {
		blockHashesIter = blockHashesDB.NewIterator()
	} else {
		blockHashesIter = blockHashesDB.NewIteratorWithRange([]byte(strconv.Itoa(start)), []byte(strconv.Itoa(limit)))
	}

	var blocks []Block
	for blockHashesIter.Next() {
		currBlockHash := blockHashesIter.Value()
		blockByte, err := blockDB.Get(currBlockHash)
		if err != nil {
			return nil, err
		}

		var blockItem Block
		err = json.Unmarshal(blockByte, &blockItem)
		if err != nil {
			return nil, err
		}

		blocks = append(blocks, blockItem)
	}

	return blocks, nil
}

// GetAllBlockHashes returns hashes of all blocks
func GetAllBlockHashes() []string {
	blockHashesIter := blockHashesDB.NewIterator()
	var blockHashesResp []string
	for blockHashesIter.Next() {
		blockHashesResp = append(blockHashesResp, string(blockHashesIter.Value()))
	}

	return blockHashesResp
}

// GetBlockByHash returns block by its hash
func GetBlockByHash(hash string) (Block, error) {
	var blockItem Block
	blockByte, err := blockDB.Get([]byte(hash))

	json.Unmarshal(blockByte, &blockItem)
	return blockItem, err
}

// GetBlockByIndex returns block by its index
func GetBlockByIndex(index string) (Block, error) {
	hash, err := blockHashesDB.Get([]byte(index))
	var block Block
	if err == nil {
		blockItem, err := blockDB.Get([]byte(hash))
		if err == nil {
			json.Unmarshal(blockItem, &block)
		}
	}
	return block, err
}

// InitLatestBlock check DB for empty
// If empty saves genesis block in DB
func InitLatestBlock() {
	// Initiate genesis block
	genesisBlock = getGenesisBlock()
	if err := genesisBlock.VerifySign(); err != nil {
		log.Fatalln(err)
	}

	// Get latest block from DB
	blHashesIter := blockHashesDB.NewIterator()
	blHashesIter.Last()

	latestBlockHash := blHashesIter.Value()
	if len(latestBlockHash) == 0 { // if db is empty
		SaveTXs(genesisBlock.Transactions)
		genesisBlock.SaveBlock()

		SetLatestBlock(genesisBlock)

		return
	}

	latestBlockByte, err := blockDB.Get(latestBlockHash)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(latestBlockByte, &latestBlock)
	if err != nil {
		panic(err)
	}
}
