package protocol

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/mitchellh/mapstructure"

	"gitlab.com/seotlt/golang_bch/protocol/helper"

	"gitlab.com/seotlt/golang_bch/db"
	"gitlab.com/seotlt/golang_bch/protocol/wallet"
)

// Token is the stucture for Token transaction
type Token struct {
	Transaction `mapstructure:",squash"`
	TxData      TokenData `json:"data" mapstructure:"data"`
}

// TokenData stucture for token data
type TokenData struct {
	Action       int8        `json:"action" mapstructure:"action"`
	ActionFields interface{} `json:"fields" mapstructure:"fields"`
	TokenName    string      `json:"name" mapstructure:"name"`
}

// CreateAction is a fields for "Create" action
type CreateAction struct {
	Mintable  int8   `json:"mintable" mapstructure:"mintable"`
	UnitLong  string `json:"ulong" mapstructure:"ulong"`
	UnitShort string `json:"ushort" mapstructure:"ushort"`
	Decimal   int    `json:"decimal" mapstructure:"decimal"`
	Supply    int    `json:"supply" mapstructure:"supply"`
}

// TransferAction is a fields for "Transfer" action
type TransferAction struct {
	Inputs  []Input  `json:"inputs" mapstructure:"inputs"`
	Outputs []Output `json:"outputs" mapstructure:"outputs"`
}

// TokenInfo is a fields for "Create" action
type TokenInfo struct {
	From      string
	Mintable  int8
	UnitLong  string
	UnitShort string
	Decimal   int
	Supply    string
}

const (
	tokenCommission = 1
)

var (
	tokenDB = db.GetInstance("tokens")
)

// NewToken is a constructor for Token
func NewToken(
	hash string,
	from string,
	txType int8,
	commission int,
	inputs []Input,
	outputs []Output,
	data TokenData,
	publicKey string,
	timestamp int64,
	signature string,
) *Token {
	tx := new(Token)
	tx.initBasicAttr(hash, from, txType, commission, inputs, outputs, publicKey, timestamp, signature)
	tx.TxData, tx.Transaction.TxData = data, data

	return tx
}

// IsValid checks transaction
func (tk Token) IsValid() error {
	if err := tk.Transaction.IsValid(); err != nil {
		return err
	}

	if err := tk.checkTokenFields(); err != nil {
		return err
	}

	return nil
}

// CheckMempool checks availability to pass transaction in Mempool
func (tk Token) CheckMempool(mempool map[string]TransactionInt) error {
	if err := tk.Transaction.CheckMempool(mempool); err != nil {
		return err
	}

	switch tokenFields := tk.TxData.ActionFields.(type) {
	case CreateAction:
		for _, tx := range mempool {
			txStruct := tx.GetStruct()

			data, ok := txStruct.TxData.(TokenData)
			if !ok {
				continue
			}

			if data.Action != 0 {
				continue
			}

			if data.TokenName == tk.TxData.TokenName {
				errStr := fmt.Sprintf("Transaction creating token %s already in mempool", data.TokenName)
				return errors.New(errStr)
			}
		}
	case TransferAction:
		inputs := make(map[string]int)
		for _, in := range tokenFields.Inputs {
			key := fmt.Sprintf("%s_%d", in.TxHash, in.Index)
			inputs[key] = in.Amount
		}

		for _, tx := range mempool {
			txStruct := tx.GetStruct()
			if txStruct.From != tk.From {
				continue
			}

			data, ok := txStruct.TxData.(TokenData)
			if !ok {
				continue
			}

			if data.Action != 1 {
				continue
			}

			if tk.TxData.TokenName != data.TokenName {
				continue
			}

			actionFields := data.ActionFields.(TransferAction)
			for _, in := range actionFields.Inputs {
				key := fmt.Sprintf("%s_%d", in.TxHash, in.Index)
				if _, has := inputs[key]; has {
					return errors.New("Attempt to use same inputs")
				}
			}
		}
	}

	return nil
}

// SaveTX needs to save tx in DB
func (tk Token) SaveTX() error {
	if err := tk.Transaction.SaveTX(); err != nil {
		return err
	}

	if tk.TxData.Action == 0 {
		key := []byte(tk.Hash)
		go tokenDB.Put([]byte(tk.TxData.TokenName), key)
	}

	tk.saveUnspentInputs()

	return nil
}

func (tk Token) saveUnspentInputs() {
	if tk.TxData.Action == 0 {
		upInput := Input{
			TxHash: tk.Hash,
			Index:  0,
			Amount: tk.TxData.ActionFields.(CreateAction).Supply,
		}

		key := fmt.Sprintf("%s%s_%s_%d", tk.From, tk.TxData.TokenName, tk.Hash, 0)
		value, _ := json.Marshal(upInput)

		upInputsDB.Put([]byte(key), value)

		return
	}

	data := tk.TxData.ActionFields.(TransferAction)
	inputs := data.Inputs
	for _, input := range inputs {
		key := fmt.Sprintf("%s%s_%s_%d", tk.From, tk.TxData.TokenName, input.TxHash, input.Index)
		upInputsDB.Delete([]byte(key))
	}
	outputs := data.Outputs

	for i, output := range outputs {
		upInput := Input{
			TxHash: tk.Hash,
			Index:  i,
			Amount: output.Amount,
		}

		key := fmt.Sprintf("%s%s_%s_%d", output.Address, tk.TxData.TokenName, tk.Hash, i)
		value, _ := json.Marshal(upInput)

		upInputsDB.Put([]byte(key), value)
	}
}

func (tk Token) checkTransferCommission() error {
	tkInfo, err := GetTokenInfo(tk.TxData.TokenName)
	if err != nil {
		return err
	}
	if tk.From == tkInfo.From {
		return nil
	}

	outputs := tk.TxData.ActionFields.(TransferAction).Outputs

	sendAmount := outputs[0].Amount
	commission := computeCommission(sendAmount)
	commissionOutput := outputs[len(outputs)-1]

	if commissionOutput.Address != tkInfo.From {
		return errors.New("Invalid commission recipient")
	}
	if commissionOutput.Amount != commission {
		return errors.New("Invalid commission amount")
	}

	return nil
}

func (tk Token) checkCommission() error {
	if tk.Commission != tokenCommission {
		return errors.New("Commission for token is wrong")
	}

	var inputsSum int
	for _, in := range tk.Inputs {
		inputsSum += in.Amount
	}

	if tk.Outputs[0].Amount != inputsSum-tokenCommission {
		return errors.New("Commission for token transaction haven't paid")
	}

	return nil
}

func (tk Token) checkTokenFields() error {
	data := tk.TxData
	if data.TokenName == "" {
		return errors.New("Token transaction must include token name")
	}

	if err := tk.checkCommission(); err != nil {
		return err
	}

	switch data.Action {
	case 0: //Create
		if len(data.TokenName) < 2 {
			return errors.New("Token name length must be greater than 2")
		}

		if _, err := tokenDB.Get([]byte(data.TokenName)); err == nil {
			return errors.New("Token " + data.TokenName + " already exist")
		}

		actionFields := data.ActionFields.(CreateAction)

		if actionFields.UnitLong == "" {
			return errors.New("Transaction of token creation must include long unit name")
		}

		if actionFields.UnitShort == "" {
			return errors.New("Transaction of token creation must include short unit name")
		}

		if len(actionFields.UnitShort) > 3 {
			return errors.New("Short unit name of token must be less than or equal 3")
		}

	case 1: //Transfer
		actionFields := data.ActionFields.(TransferAction)
		if err := isInputsMore(actionFields.Inputs, actionFields.Outputs); err != nil {
			return err
		}

		if err := checkDoubleWaste(tk.From, actionFields.Inputs, data.TokenName); err != nil {
			return err
		}

		if err := tk.checkTransferCommission(); err != nil {
			return err
		}

	case 2: //Supply
		tkInfo, err := GetTokenInfo(data.TokenName)
		if err != nil {
			return errors.New("Can't get token info: " + err.Error())
		}
		if tk.From != tkInfo.From {
			return errors.New("You must be an author of the token")
		}

		if !isTokenMintable(tkInfo) {
			return errors.New("Token " + data.TokenName + " can't be supplied as it doesn't mintable")
		}
	}

	return nil
}

func getTokenCreateTX(tkName string) (TransactionInt, error) {
	txHash, err := tokenDB.Get([]byte(tkName))
	if err != nil {
		return nil, err
	}

	infoTX, err := txDB.Get(txHash)
	if err != nil {
		return nil, err
	}

	parsedTX, err := JSONToTransaction(infoTX)
	if err != nil {
		return nil, err
	}

	return parsedTX, nil
}

// GetTokenInfo returns TokenInfo struct with information about specified token
func GetTokenInfo(tkName string) (TokenInfo, error) {
	parsedTX, err := getTokenCreateTX(tkName)
	if err != nil {
		return TokenInfo{}, nil
	}

	txStruct := parsedTX.GetStruct()
	data := txStruct.TxData.(TokenData)
	actionFields := data.ActionFields.(CreateAction)

	supplyInt := getTotalSupply(tkName, actionFields.Supply)
	supplyStr := strconv.Itoa(supplyInt)
	index := len(supplyStr) - actionFields.Decimal
	supply := supplyStr[:index] + "." + supplyStr[index:]

	tkInfo := TokenInfo{
		From:      txStruct.From,
		Mintable:  actionFields.Mintable,
		UnitLong:  actionFields.UnitLong,
		UnitShort: actionFields.UnitShort,
		Decimal:   actionFields.Decimal,
		Supply:    supply,
	}

	return tkInfo, nil
}

func getTotalSupply(tkName string, initSupply int) int {
	totalSupply := initSupply
	txIter := txDB.NewIterator()
	for txIter.Next() {
		tx, _ := JSONToTransaction(txIter.Value())
		tokenData, ok := tx.GetStruct().TxData.(TokenData)
		if !ok || tx.GetStruct().TxType != 2 {
			continue
		}

		if tokenData.TokenName != tkName {
			continue
		}

		if tokenData.Action != 2 {
			continue
		}

		var fields TransferAction
		mapstructure.Decode(tokenData.ActionFields, &fields)
		additSupply := fields.Outputs[0].Amount
		totalSupply = totalSupply + additSupply
	}

	return totalSupply
}

// GenToken generates new Token transaction
func GenToken(from wallet.Wallet, to string, txType int8, optParams map[string]string) (*Token, error) {
	if _, has := optParams["tkName"]; !has {
		return nil, errors.New("Token name argument is necessary for actions with token")
	}
	action, _ := strconv.Atoi(optParams["action"])

	var actionFields interface{}
	switch action {
	case 0: //Create
		decimal, _ := strconv.Atoi(optParams["decimal"])
		supplyFloat, _ := strconv.ParseFloat(optParams["supply"], 64)
		supply, err := helper.ConvertFloatToInt(supplyFloat, decimal)
		if err != nil {
			return nil, err
		}

		mintable, _ := strconv.Atoi(optParams["mintable"])
		actionFields = CreateAction{
			Mintable:  int8(mintable),
			UnitLong:  optParams["unitLong"],
			UnitShort: optParams["unitShort"],
			Decimal:   decimal,
			Supply:    supply,
		}
	case 1: //Transfer
		tkInfo, err := GetTokenInfo(optParams["tkName"])
		if err != nil {
			return nil, err
		}

		amountFloat, _ := strconv.ParseFloat(optParams["amount"], 64)
		amount, err := helper.ConvertFloatToInt(amountFloat, tkInfo.Decimal)
		if err != nil {
			return nil, err
		}
		var commission int
		if from.Address != tkInfo.From {
			commission = computeCommission(amount)
		}

		inputs, remainder, err := FormInputs(txType, optParams["tkName"], from.Address, amount+commission)
		if err != nil {
			return nil, err
		}
		outputs := FormOutputs(from.Address, to, amount, remainder)

		if from.Address != tkInfo.From {
			commissionOutput := Output{
				Address: tkInfo.From,
				Amount:  commission,
			}
			outputs = append(outputs, commissionOutput)
		}

		actionFields = TransferAction{
			Inputs:  inputs,
			Outputs: outputs,
		}
	case 2: //Supply
		tkInfo, err := GetTokenInfo(optParams["tkName"])
		if err != nil {
			return nil, err
		}

		amountFloat, _ := strconv.ParseFloat(optParams["amount"], 64)
		amount, err := helper.ConvertFloatToInt(amountFloat, tkInfo.Decimal)
		if err != nil {
			return nil, err
		}

		outputs := []Output{
			Output{
				Address: from.Address,
				Amount:  amount,
			},
		}
		actionFields = TransferAction{
			Inputs:  nil,
			Outputs: outputs,
		}
	}

	data := TokenData{
		Action:       int8(action),
		TokenName:    optParams["tkName"],
		ActionFields: actionFields,
	}

	coinInputs, remainder, err := FormInputs(0, "", from.Address, tokenCommission)
	if err != nil {
		return nil, err
	}

	coinOutputs := FormOutputs(from.Address, from.Address, remainder, 0)

	return NewToken("", from.Address, txType, tokenCommission, coinInputs, coinOutputs, data, from.PublicKey, 0, ""), nil
}

// GetUserTokens returns map with user tokens and its balance
func GetUserTokens(address string) map[string]string {
	tokenBalances := make(map[string]int)

	upInputsIterator := upInputsDB.NewIteratorWithPrefix([]byte(address))
	for upInputsIterator.Next() {
		key := string(upInputsIterator.Key())
		keyParts := strings.Split(key, "_")
		tokenName := keyParts[0][42:]

		if len(tokenName) < 2 {
			continue
		}

		var inputInfo Input
		json.Unmarshal(upInputsIterator.Value(), &inputInfo)

		tokenBalances[tokenName] += inputInfo.Amount
	}

	formatedBalances := make(map[string]string)
	for tkName, balance := range tokenBalances {
		createTokenTX, _ := getTokenCreateTX(tkName)
		tokenData := createTokenTX.GetStruct().TxData.(TokenData)
		createFields := tokenData.ActionFields.(CreateAction)

		decimal := createFields.Decimal
		balanceStr := strconv.Itoa(balance)
		dotIndex := len(balanceStr) - decimal
		formatedBalance := balanceStr[:dotIndex] + "." + balanceStr[dotIndex:] + " " + createFields.UnitShort

		formatedBalances[tkName] = formatedBalance
	}

	return formatedBalances
}

func isTokenMintable(tkInfo TokenInfo) bool {
	return tkInfo.Mintable == 1
}
