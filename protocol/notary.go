package protocol

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/seotlt/golang_bch/protocol/helper"
	"gitlab.com/seotlt/golang_bch/protocol/wallet"
)

// Notary is the stucture for Notary transaction
type Notary struct {
	Transaction `mapstructure:",squash"`
	TxData      NotaryData `json:"data" mapstructure:"data"`
}

const (
	notaryCommission = 1
)

// NotaryData stucture for voting data
type NotaryData struct {
	Files map[string]string `json:"files" mapstructure:"files"`
}

// NewNotary is a constructor for Notary
func NewNotary(
	hash string,
	from string,
	txType int8,
	commission int,
	inputs []Input,
	outputs []Output,
	data NotaryData,
	publicKey string,
	timestamp int64,
	signature string,
) *Notary {
	tx := new(Notary)
	tx.initBasicAttr(hash, from, txType, commission, inputs, outputs, publicKey, timestamp, signature)
	tx.TxData, tx.Transaction.TxData = data, data

	return tx
}

// IsValid checks transaction
func (nt Notary) IsValid() error {
	if err := nt.Transaction.IsValid(); err != nil {
		return err
	}

	if err := nt.checkCommission(); err != nil {
		return err
	}

	return nil
}

func (nt Notary) checkCommission() error {
	if nt.Commission != notaryCommission {
		return errors.New("Commission for notary is wrong")
	}

	var inputsSum int
	for _, in := range nt.Inputs {
		inputsSum += in.Amount
	}

	if nt.Outputs[0].Amount != inputsSum-notaryCommission {
		return errors.New("Commission for notary transaction hasn't been paid")
	}

	return nil
}

// SaveTX needs to save tx in DB
func (nt Notary) SaveTX() error {
	if err := nt.Transaction.SaveTX(); err != nil {
		return err
	}

	return nil
}

// GenNotary generates new Notary transaction
func GenNotary(from wallet.Wallet, txType int8, optParams map[string]string) (*Notary, error) {
	filePaths := strings.Split(optParams["filePaths"], " ")
	files := make(map[string]string)
	for _, filePath := range filePaths {
		fileHash, err := hashFile(filePath)
		if err != nil {
			return nil, err
		}
		fileName := filepath.Base(filePath)

		files[fileName] = fileHash
	}

	data := NotaryData{
		Files: files,
	}

	inputs, remainder, err := FormInputs(0, "", from.Address, notaryCommission)
	if err != nil {
		return nil, err
	}
	outputs := FormOutputs(from.Address, from.Address, remainder, 0)

	return NewNotary("", from.Address, txType, notaryCommission, inputs, outputs, data, from.PublicKey, 0, ""), nil
}

// CheckNotary checks notary contract
func CheckNotary(hash, filePaths string) (bool, error) {
	notaryTX, err := txDB.Get([]byte(hash))
	if err != nil {
		return false, err
	}

	parsedTX, err := JSONToTransaction(notaryTX)
	if err != nil {
		return false, err
	}
	txStruct := parsedTX.GetStruct()
	data := txStruct.TxData.(NotaryData)

	fi, err := os.Stat(filePaths)
	if err != nil {
		return false, err
	}

	switch mode := fi.Mode(); {
	case mode.IsDir():
		checked, err := checkDirectory(filePaths, data.Files)
		if err != nil {
			return false, err
		}

		return checked, nil
	case mode.IsRegular():
		fileHash, err := hashFile(filePaths)
		if err != nil {
			return false, err
		}

		if fileHash != data.Files[fi.Name()] {
			return false, nil
		}

		return true, nil
	}

	return false, nil
}

func checkDirectory(filePaths string, notaryFiles map[string]string) (bool, error) {
	if !strings.HasSuffix(filePaths, "/") {
		filePaths += "/"
	}

	files, err := ioutil.ReadDir(filePaths)
	if err != nil {
		return false, err
	}

	filesCount := len(notaryFiles)
	for _, file := range files {
		filename := file.Name()
		if _, has := notaryFiles[filename]; !has {
			continue
		}

		fileHash, err := hashFile(filePaths + filename)
		if err != nil {
			continue
		}

		if fileHash != notaryFiles[filename] {
			return false, nil
		}

		filesCount--
		if filesCount == 0 {
			return true, nil
		}
	}

	return false, nil
}

func hashFile(filePath string) (string, error) {
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		errStr := fmt.Sprintf("File %s does not exist!", filePath)
		return "", errors.New(errStr)
	}

	fileData, err := ioutil.ReadFile(filePath)
	if err != nil {
		errStr := fmt.Sprintf("Failed to read file %s: %s", filePath, err.Error())
		return "", errors.New(errStr)
	}

	fileHash := helper.CalcHash(fileData)

	return fileHash, nil
}
