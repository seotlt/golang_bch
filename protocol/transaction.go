package protocol

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/crypto"
	"gitlab.com/seotlt/golang_bch/db"
	"gitlab.com/seotlt/golang_bch/protocol/helper"
	"gitlab.com/seotlt/golang_bch/protocol/wallet"
)

var (
	txDB = db.GetInstance("txs")
)

// TransactionInt provide interface for all transaction types
type TransactionInt interface {
	SaveTX() error
	SignTX(string) error
	IsValid() error
	CheckMempool(map[string]TransactionInt) error
	GetHash() string
	GetStruct() Transaction
	GenTXHash() string
}

// Transaction is a struct representation of Transaction
type Transaction struct {
	Hash       string      `json:"hash,omitempty" mapstructure:"hash"`
	From       string      `json:"from" mapstructure:"from"`
	TxData     interface{} `json:"data"`
	TxType     int8        `json:"type" mapstructure:"type"`
	Commission int         `json:"commission" mapstructure:"commission"`
	Inputs     []Input     `json:"inputs" mapstructure:"inputs"`
	Outputs    []Output    `json:"outputs" mapstructure:"outputs"`
	PublicKey  string      `json:"pubkey" mapstructure:"pubKey"`
	Timestamp  int64       `json:"timestamp,omitempty" mapstructure:"timestamp"`
	Signature  string      `json:"signature,omitempty" mapstructure:"signature"`
}

// SaveTX needs to save tx in DB
func (tx Transaction) SaveTX() error {
	key := []byte(tx.Hash)
	txJSON, err := json.Marshal(tx)
	if err != nil {
		return err
	}

	txDB.Put(key, txJSON)

	tx.saveUnspentInputs()

	return nil
}

func (tx Transaction) saveUnspentInputs() {
	txType := tx.TxType
	if txType > 1 {
		txType = 0
	}

	inputs := tx.Inputs
	for _, input := range inputs {
		key := fmt.Sprintf("%s%d_%s_%d", tx.From, txType, input.TxHash, input.Index)
		upInputsDB.Delete([]byte(key))
	}

	outputs := tx.Outputs
	for i, output := range outputs {
		upInput := Input{
			TxHash: tx.Hash,
			Index:  i,
			Amount: output.Amount,
		}

		key := fmt.Sprintf("%s%d_%s_%d", output.Address, txType, tx.Hash, i)
		value, _ := json.Marshal(upInput)

		upInputsDB.Put([]byte(key), value)
	}
}

// IsValid checks transaction
func (tx Transaction) IsValid() error {
	if err := tx.checkFields(); err != nil {
		return err
	}

	if err := tx.verifyTXSign(); err != nil {
		return err
	}

	return nil
}

// CheckMempool checks availability to pass transaction in Mempool
func (tx Transaction) CheckMempool(mempool map[string]TransactionInt) error {
	// Already in mempool?
	if _, has := mempool[tx.Hash]; has {
		err := fmt.Sprintf("Transaction with hash %s already in mempool!", tx.Hash)
		return errors.New(err)
	}

	inputs := make(map[string]int)
	for _, in := range tx.Inputs {
		key := fmt.Sprintf("%s_%d", in.TxHash, in.Index)
		inputs[key] = in.Amount
	}

	for _, txMem := range mempool {
		txStruct := txMem.GetStruct()
		if txStruct.From != tx.From {
			continue
		}

		for _, in := range txStruct.Inputs {
			key := fmt.Sprintf("%s_%d", in.TxHash, in.Index)
			if _, has := inputs[key]; has {
				return errors.New("Attempt to use same inputs")
			}
		}
	}

	return nil
}

// SignTX is a function for sign the transaction
func (tx *Transaction) SignTX(privateKeyStr string) error {
	privateKey, err := crypto.HexToECDSA(privateKeyStr) //convert to ecdsa.PrivateKey
	if err != nil {
		return err
	}
	txJSON, err := json.Marshal(tx)
	if err != nil {
		return err
	}

	msgHash := sha256.Sum256(txJSON) //hash tx struct

	signature, err := crypto.Sign(msgHash[:], privateKey)
	if err != nil {
		return err
	}

	signatureStr := hex.EncodeToString(signature)
	tx.Signature = signatureStr

	tx.Hash = tx.GenTXHash() //gen txhash after creation of signature

	return nil
}

// GenTXHash generate and returns hash of transaction
func (tx Transaction) GenTXHash() string {
	// create copy of tx and remove Timestamp field
	// it's not important to hash it because we already
	// used it in Signature
	tx.Timestamp = 0
	tx.Hash = ""

	txJSON, err := json.Marshal(tx)
	if err != nil {
		log.Println(err)
	}

	return helper.CalcHash(txJSON)
}

// GetHash returns hash of transaction
func (tx Transaction) GetHash() string {
	return tx.Hash
}

// GetStruct returns structure of Transaction struct
func (tx Transaction) GetStruct() Transaction {
	return tx
}

func (tx Transaction) checkFields() error {
	generatedHash := tx.GenTXHash()
	if generatedHash != tx.Hash {
		log.Println(generatedHash, tx.Hash)
		return errors.New("Hash of transaction do not equal to the generated hash")
	}

	if err := wallet.CheckAddress(tx.From); err != nil {
		return err
	}

	return nil
}

func (tx Transaction) verifyTXSign() error {
	signature := tx.Signature

	// remove Signature field
	// because in SignTX we didn't use this field
	tx.Signature = ""
	tx.Hash = ""

	publicKey, err := hex.DecodeString("04" + tx.PublicKey) //to bytes arr
	if err != nil {
		return err
	}

	// Check conforming public key and 'from' address
	pubKeyECDSA, err := crypto.UnmarshalPubkey(publicKey)
	if err != nil {
		return err
	}
	addrFromPub := crypto.PubkeyToAddress(*pubKeyECDSA).Hex()

	if strings.ToLower(addrFromPub) != tx.From {
		return errors.New("Public key in transaction doesn't conform to 'from' address")
	}

	txJSON, err := json.Marshal(tx)
	if err != nil {
		return err
	}

	msgHash := sha256.Sum256(txJSON)
	byteSignature, err := hex.DecodeString(signature)
	if err != nil {
		return err
	}

	// IMPORTANT: remove recovery ID (without last byte)
	signatureNoRecoverID := byteSignature[:len(byteSignature)-1]
	valid := crypto.VerifySignature(publicKey, msgHash[:], signatureNoRecoverID)
	if !valid {
		return errors.New("Invalid signature of transaction")
	}

	return nil
}

func (tx *Transaction) initBasicAttr(
	hash string,
	from string,
	txType int8,
	commission int,
	inputs []Input,
	outputs []Output,
	publicKey string,
	timestamp int64,
	signature string,
) {
	tx.From = from
	tx.PublicKey = publicKey

	tx.TxType = txType
	tx.Commission = commission
	tx.Inputs = inputs
	tx.Outputs = outputs

	if timestamp == 0 {
		timestamp = time.Now().UnixNano() / 1000000
	}
	tx.Timestamp = timestamp

	if signature != "" {
		tx.Signature = signature
	}

	if hash != "" {
		tx.Hash = hash
	} else if tx.Signature != "" {
		tx.GenTXHash()
	}
}

// SaveTXs saves every tx in txs slice
func SaveTXs(txs []TransactionInt) error {
	for _, tx := range txs {
		if err := tx.SaveTX(); err != nil {
			return err
		}
	}

	return nil
}

// GetTxByHash returns transaction by hash
func GetTxByHash(hash string) (TransactionInt, error) {
	txData, err := txDB.Get([]byte(hash))
	if err != nil {
		return nil, err
	}
	return JSONToTransaction(txData)
}

// GetAllTxs returns all transactions
func GetAllTxs() []TransactionInt {
	txsIter := txDB.NewIterator()
	var txsResp []TransactionInt
	for txsIter.Next() {
		txItem, _ := JSONToTransaction(txsIter.Value())
		txsResp = append(txsResp, txItem)
	}
	return txsResp
}
