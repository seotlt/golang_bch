package protocol

import (
	"errors"
	"fmt"
)

var transactions = make(map[string]TransactionInt)

// AddToMempool is a func for adding new transaction to the mempool
func AddToMempool(tx TransactionInt) {
	transactions[tx.GetHash()] = tx
}

// GetMempoolTxs returns all transactions from mempool
func GetMempoolTxs(countOptional ...int) ([]TransactionInt, int) {
	var txItems []TransactionInt
	var fees int
	count := len(transactions)

	if len(countOptional) > 0 {
		count = countOptional[0]
	}

	i := 0
	for _, v := range transactions {
		if i == count {
			break
		}

		txItems = append(txItems, v)
		fees += v.GetStruct().Commission

		i++
	}

	return txItems, fees
}

// GetMempoolTx returns transaction from mempool by its hash
func GetMempoolTx(hash string) TransactionInt {
	return transactions[hash]
}

// GetTransactionsForBlock selects and returns transactions
// for new block from mempool
func GetTransactionsForBlock(count int) ([]TransactionInt, int) {
	txs, fees := GetMempoolTxs(count)

	go removeTXsFromMempool(txs)

	return txs, fees
}

// AlreadyInMempool checks is transaction in mempool or not
func AlreadyInMempool(txHash string) bool {
	_, isExist := transactions[txHash]

	return isExist
}

// ClearMempool truncates mempool
func ClearMempool() {
	transactions = make(map[string]TransactionInt)
}

// CheckTransaction checks transaction before adding to memppol
func CheckTransaction(tx TransactionInt, isMine bool) error {
	if AlreadyInMempool(tx.GetHash()) {
		err := fmt.Sprintf("Transaction with hash %s already in mempool!", tx.GetHash())
		return errors.New(err)
	}

	if isMine {
		return nil
	}

	if err := tx.CheckMempool(transactions); err != nil {
		return err
	}

	if err := tx.IsValid(); err != nil {
		return err
	}

	return nil
}

func removeTXsFromMempool(txs []TransactionInt) {
	for _, tx := range txs {
		delete(transactions, tx.GetHash())
	}
}
