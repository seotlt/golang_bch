package forging

import (
	"errors"
	"log"
	"math"
	"strconv"
	"time"

	"gitlab.com/seotlt/golang_bch/protocol/wallet"

	"gitlab.com/seotlt/golang_bch/network/node"
	"gitlab.com/seotlt/golang_bch/protocol"
	"gitlab.com/seotlt/golang_bch/protocol/helper"
)

const maxSizeBlock = 4000

var (
	Forging    = false
	genTime    = 0
	forgeTimer *time.Timer
	forger     wallet.Wallet
)

// StopForging - stop forging
func StopForging() {
	if forgeTimer != nil {
		forgeTimer.Stop()
	}
	Forging = false

	forger = wallet.Wallet{}
}

// StartForging - start forging on selected wallet
func StartForging(forgerWallet wallet.Wallet) error {
	if forgeTimer != nil {
		forgeTimer.Stop()
	}

	if forgerWallet.Address == "" || forgerWallet.PublicKey == "" {
		if forger.Address == "" || forger.PublicKey == "" {
			return errors.New("You must transfer address and pubkey to start forging")
		}
	} else {
		forger = forgerWallet
	}

	statesAmount := protocol.GetBalance(1, "", forgerWallet.Address)
	if statesAmount == 0 {
		return errors.New("Can't forge with zero state")
	}

	Forging = true

	go forge()

	return nil
}

func GetForgeTime() int {
	return genTime
}

func forge() {
	if !Forging {
		return
	}

	latestBlock := protocol.GetLatestBlock()
	targetPerSec := getTarget(forger.Address, latestBlock.BaseTarget)

	if targetPerSec <= 0 {
		log.Println(`Can't forge with zero state!`, targetPerSec)

		StopForging()
		return
	}

	if node.State == 1 {
		log.Println("Wait synced")
		<-*node.WaitSync()
		log.Println("synced")
	}

	timestamp := int(time.Now().Unix()) //In sec
	log.Println(timestamp-latestBlock.Timestamp, targetPerSec)
	currTarget := targetPerSec * (timestamp - latestBlock.Timestamp)
	log.Printf(`target: %d`, currTarget)

	genSig := getGenSignature(forger.PublicKey, latestBlock.GenSignature)
	hit := getHit(genSig)
	log.Printf(`hit: %d`, hit)

	genTime = 0
	if currTarget < int(hit) {
		genTime = (int(hit)-currTarget)/targetPerSec + 1 //In sec
	}
	log.Printf(`Next block will be generated in %d сек.`, genTime)

	// старт перерасчета по таймеру
	forgeTimer = time.NewTimer(time.Duration(genTime) * time.Second)
	<-forgeTimer.C
	if node.State == 1 {
		forge()
	}
	start := time.Now() //начало отсчета времени форжинга блока

	transactions, fees := protocol.GetTransactionsForBlock(maxSizeBlock)
	log.Printf(`Собрано в блок %d транзакций`, len(transactions))

	if fees > 0 {
		params := make(map[string]string)
		params["fee"] = "true"
		params["amount"] = strconv.Itoa(fees)
		coinbase, err := protocol.GenCoin(forger, forger.Address, 0, params)
		if err != nil {
			log.Println("Can't create coinbase transaction: ", err)
			StopForging()
			return
		}
		if err := coinbase.SignTX(forger.PrivateKey); err != nil {
			log.Println("Can't sign coinbase tx: ", err)
			StopForging()
			return
		}
		transactions = append([]protocol.TransactionInt{coinbase}, transactions...)
	}

	latestBlock = protocol.GetLatestBlock()

	newTimestamp := int(time.Now().Unix())
	maxTarget := math.Min(
		2*float64(latestBlock.BaseTarget),
		float64(protocol.MaxBaseTarget),
	)
	minTarget := math.Max(
		math.Floor(float64(latestBlock.BaseTarget)/2),
		1,
	)
	delta := newTimestamp - latestBlock.Timestamp
	candidate := math.Floor(float64(latestBlock.BaseTarget) * float64(delta) / 10)
	baseTarget := math.Min(
		math.Max(minTarget, candidate),
		maxTarget,
	)

	newBlock := protocol.GenNextBlock(transactions, forger.Address, forger.PublicKey, int(baseTarget), genSig)

	if err := protocol.IsValidNextBlock(newBlock, &latestBlock); err != nil {
		log.Println("Invalid block: ", err)

		StopForging()
		log.Println("Forging has been stopped")

		return
	}

	go protocol.SaveTXs(transactions)

	protocol.SetLatestBlock(*newBlock)
	node.SendBlock(newBlock)

	newBlock.SaveBlock()

	log.Printf("Блок сгенерировался за %fс", time.Since(start).Seconds())

	time.Sleep(time.Second)
	forge()
}

// GetGenSignature returns generation signature for specified public key
func getGenSignature(publicKey, lstBlckGenSign string) string {
	return helper.CalcHash([]byte(lstBlckGenSign + publicKey))
}

// GetTarget returns target for specified address
func getTarget(address string, lstBlckBaseTarg int) int {
	state := protocol.GetBalance(1, "", address)
	log.Println("latestBlock base target: ", lstBlckBaseTarg)
	return lstBlckBaseTarg * state
}

// GetHit returns hit of genSig
// maybe in 'hex'?
func getHit(genSig string) int64 {
	intDes, err := strconv.ParseInt(genSig[0:12], 16, 64)
	if err != nil {
		log.Println(err)
		return 0
	}

	return intDes
}
