package protocol

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/seotlt/golang_bch/db"

	"gitlab.com/seotlt/golang_bch/protocol/wallet"
)

// Voting is the stucture for Voting transaction
type Voting struct {
	Transaction `mapstructure:",squash"`
	TxData      VotingData `json:"data" mapstructure:"data"`
}

// VotingData stucture for voting data
type VotingData struct {
	Action       int8        `json:"action" mapstructure:"action"`
	ActionFields interface{} `json:"fields" mapstructure:"fields"`
}

// CreateVoting is structure contains info about new voting
type CreateVoting struct {
	Question string   `json:"question" mapstructure:"question"`
	Voters   []string `json:"voters" mapstructure:"voters"`
}

// VoteAction is structure contains info about new vote in specific voting
type VoteAction struct {
	QuestionHash string `json:"questHash" mapstructure:"questHash"`
	Answer       string `json:"answer" mapstructure:"answer"`
}

// CloseVoting contains necessary data for closing voting
type CloseVoting struct {
	QuestionHash string `json:"questHash" mapstructure:"questHash"`
}

// VotingInfo is structure contains info about specific voting
type VotingInfo struct {
	From     string
	Question string
	Voters   []string
	Closed   bool
}

// VotingResult represents results of specified voting
type VotingResult struct {
	Yes     int
	No      int
	Abstain int
	Other   int
	Voters  map[string]string
}

const (
	votingCommission = 1
)

var (
	votingDB = db.GetInstance("voting")
)

// NewVoting is a constructor for Voting
func NewVoting(
	hash string,
	from string,
	txType int8,
	commission int,
	inputs []Input,
	outputs []Output,
	data VotingData,
	publicKey string,
	timestamp int64,
	signature string,
) *Voting {
	tx := new(Voting)
	tx.initBasicAttr(hash, from, txType, commission, inputs, outputs, publicKey, timestamp, signature)
	tx.TxData, tx.Transaction.TxData = data, data

	return tx
}

// IsValid checks transaction
func (vt Voting) IsValid() error {
	if err := vt.Transaction.IsValid(); err != nil {
		return err
	}

	if err := vt.checkVotingFields(); err != nil {
		return err
	}

	return nil
}

// CheckMempool checks availability to pass transaction in Mempool
func (vt Voting) CheckMempool(mempool map[string]TransactionInt) error {
	if err := vt.Transaction.CheckMempool(mempool); err != nil {
		return err
	}

	switch voteFields := vt.TxData.ActionFields.(type) {
	case VoteAction:
		for _, tx := range mempool {
			txStruct := tx.GetStruct()

			data, ok := txStruct.TxData.(VotingData)
			if !ok {
				continue
			}

			switch actionFields := data.ActionFields.(type) {
			case VoteAction:
				if txStruct.From != vt.From {
					continue
				}

				// Already Voted?
				if actionFields.QuestionHash == voteFields.QuestionHash {
					return errors.New("Transaction with vote already in mempool")
				}
			case CloseVoting:
				// Already closed?
				if actionFields.QuestionHash == voteFields.QuestionHash {
					return errors.New("Transaction closing voting already in mempool")
				}
			default:
				continue
			}
		}
	case CloseVoting:
		for _, tx := range mempool {
			txStruct := tx.GetStruct()
			if txStruct.From != vt.From {
				continue
			}

			data, ok := txStruct.TxData.(VotingData)
			if !ok {
				continue
			}

			switch actionFields := data.ActionFields.(type) {
			case CloseVoting:
				// Already closed?
				if actionFields.QuestionHash == voteFields.QuestionHash {
					return errors.New("Transaction closing voting already in mempool")
				}
			default:
				continue
			}
		}
	}

	return nil
}

func (vt Voting) checkVotingFields() error {
	if err := vt.checkCommission(); err != nil {
		return err
	}

	data := vt.TxData
	switch data.Action {
	case 0: //Create
		actionFields := data.ActionFields.(CreateVoting)
		if len(actionFields.Question) == 0 {
			return errors.New("Question can't be empty")
		}

		if len(actionFields.Question) > 500 {
			return errors.New("Question can't be more than 500 symbols")
		}

		if len(actionFields.Voters) == 1 && actionFields.Voters[0] == "" {
			return errors.New("Voting must include at least one voter")
		}
	case 1: //Vote
		actionFields := data.ActionFields.(VoteAction)
		if isVotingClosed(actionFields.QuestionHash) {
			errStr := fmt.Sprintf("Voting %s is closed", actionFields.QuestionHash)
			return errors.New(errStr)
		}

		vtInfo, err := GetVotingInfo(actionFields.QuestionHash)
		if err != nil {
			return err
		}

		if !isAddressVoter(vtInfo.Voters, vt.From) {
			errStr := fmt.Sprintf("Address %s is not in the voters list", vt.From)
			return errors.New(errStr)
		}

		if isAlreadyVoted(actionFields.QuestionHash, vt.From) {
			errStr := fmt.Sprintf("Address %s already voted", vt.From)
			return errors.New(errStr)
		}
	case 2: //Close
		actionFields := data.ActionFields.(CloseVoting)
		vtInfo, err := GetVotingInfo(actionFields.QuestionHash)
		if err != nil {
			return err
		}

		if vtInfo.From != vt.From {
			errStr := fmt.Sprintf("Address %s can't close voting as it is not author", vt.From)
			return errors.New(errStr)
		}
	}

	return nil
}

func (vt Voting) checkCommission() error {
	if vt.TxData.Action == 1 {
		return nil
	}

	if vt.Commission != votingCommission {
		return errors.New("Commission for voting is wrong")
	}

	var inputsSum int
	for _, in := range vt.Inputs {
		inputsSum += in.Amount
	}

	if vt.Outputs[0].Amount != inputsSum-votingCommission {
		return errors.New("Commission for voting transaction hasn't been paid")
	}

	return nil
}

// SaveTX needs to save tx in DB
func (vt Voting) SaveTX() error {
	if err := vt.Transaction.SaveTX(); err != nil {
		return err
	}

	switch vt.TxData.Action {
	case 1:
		vt.saveVote()
	case 2:
		vt.saveClosing()
	}

	return nil
}

func (vt Voting) saveVote() {
	voteFields := vt.TxData.ActionFields.(VoteAction)
	key := fmt.Sprintf("%s_%s", voteFields.QuestionHash, vt.From)

	votingDB.Put([]byte(key), []byte(voteFields.Answer))
}

func (vt Voting) saveClosing() {
	key := fmt.Sprintf("c_%s", vt.TxData.ActionFields.(CloseVoting).QuestionHash)
	votingDB.Put([]byte(key), []byte(""))
}

func isVotingClosed(hash string) bool {
	key := fmt.Sprintf("c_%s", hash)
	if _, err := votingDB.Get([]byte(key)); err == nil {
		return true
	}

	return false
}

func isAddressVoter(voters []string, address string) bool {
	for _, voter := range voters {
		if voter == address {
			return true
		}
	}
	return false
}

func isAlreadyVoted(vtHash string, voter string) bool {
	key := fmt.Sprintf("%s_%s", vtHash, voter)
	if _, err := votingDB.Get([]byte(key)); err == nil {
		return true
	}

	return false
}

// GenVoting generates new Voting transaction
func GenVoting(from wallet.Wallet, txType int8, optParams map[string]string) (*Voting, error) {
	action, _ := strconv.Atoi(optParams["action"])

	commission := votingCommission
	var actionFields interface{}
	switch action {
	case 0: //Create
		voters := strings.Split(optParams["voters"], " ")
		actionFields = CreateVoting{
			Question: optParams["question"],
			Voters:   voters,
		}
	case 1: //Vote
		actionFields = VoteAction{
			QuestionHash: optParams["questHash"],
			Answer:       optParams["answer"],
		}

		commission = 0
	case 2: //Close
		actionFields = CloseVoting{
			QuestionHash: optParams["questHash"],
		}
	}

	var inputs []Input
	var outputs []Output
	if commission != 0 {
		var remainder int
		var err error
		inputs, remainder, err = FormInputs(0, "", from.Address, commission)
		if err != nil {
			return nil, err
		}
		outputs = FormOutputs(from.Address, from.Address, remainder, 0)
	}

	data := VotingData{
		Action:       int8(action),
		ActionFields: actionFields,
	}

	return NewVoting("", from.Address, txType, commission, inputs, outputs, data, from.PublicKey, 0, ""), nil
}

// GetVotingInfo returns information about voting specified by hash
func GetVotingInfo(hash string) (VotingInfo, error) {
	votingTXJson, err := txDB.Get([]byte(hash))
	if err != nil {
		return VotingInfo{}, err
	}

	votingTX, err := JSONToTransaction(votingTXJson)
	if err != nil {
		return VotingInfo{}, err
	}
	txStruct := votingTX.GetStruct()
	data := txStruct.TxData.(VotingData)
	actionFields := data.ActionFields.(CreateVoting)

	votingInfo := VotingInfo{
		From:     txStruct.From,
		Question: actionFields.Question,
		Voters:   actionFields.Voters,
		Closed:   isVotingClosed(hash),
	}

	return votingInfo, nil
}

// GetVotingResult returns result of the voting specified by hash
func GetVotingResult(hash string) VotingResult {
	votingIter := votingDB.NewIteratorWithPrefix([]byte(hash))

	votingResult := VotingResult{}
	voters := make(map[string]string)
	for votingIter.Next() {
		key := string(votingIter.Key())
		voter := key[65:]
		answer := string(votingIter.Value())

		switch answer {
		case "0":
			votingResult.Yes++
		case "1":
			votingResult.No++
		case "2":
			votingResult.Abstain++
		default:
			votingResult.Other++
		}

		voters[voter] = answer
	}

	votingResult.Voters = voters

	return votingResult
}
