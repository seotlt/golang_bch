package protocol

import (
	"encoding/json"

	"github.com/mitchellh/mapstructure"
)

var (
	txTypes = []string{
		"Coin",
		"State",
		"Token",
		"Voting",
		"Notary",
	}
)

// JSONToTransaction parses transaction from json to Transaction struct
func JSONToTransaction(txByte []byte) (TransactionInt, error) {
	var txMap map[string]interface{}
	if err := json.Unmarshal(txByte, &txMap); err != nil {
		return nil, err
	}

	resTX := txMapToTX(txMap)
	return resTX, nil
}

// GetTXTypes returns all transaction names
func GetTXTypes() []string {
	return txTypes
}

func txMapToTX(txMap map[string]interface{}) TransactionInt {
	txType := int(txMap["type"].(float64))

	var resTX TransactionInt
	switch txType {
	case 0:
		parsedTX := new(Coin)
		mapstructure.Decode(txMap, parsedTX)
		parsedTX.Transaction.TxData = parsedTX.TxData

		resTX = parsedTX
	case 1:
		parsedTX := new(Coin)
		mapstructure.Decode(txMap, parsedTX)
		parsedTX.Transaction.TxData = parsedTX.TxData

		resTX = parsedTX
	case 2:
		parsedTX := new(Token)
		mapstructure.Decode(txMap, parsedTX)

		switch parsedTX.TxData.Action {
		case 0:
			var createFields CreateAction
			mapstructure.Decode(parsedTX.TxData.ActionFields, &createFields)
			parsedTX.TxData.ActionFields = createFields
		case 1:
			var transferFields TransferAction
			mapstructure.Decode(parsedTX.TxData.ActionFields, &transferFields)
			parsedTX.TxData.ActionFields = transferFields
		case 2:
			var transferFields TransferAction
			mapstructure.Decode(parsedTX.TxData.ActionFields, &transferFields)
			parsedTX.TxData.ActionFields = transferFields
		}

		parsedTX.Transaction.TxData = parsedTX.TxData

		resTX = parsedTX
	case 3:
		parsedTX := new(Voting)
		mapstructure.Decode(txMap, parsedTX)

		switch parsedTX.TxData.Action {
		case 0:
			var createFields CreateVoting
			mapstructure.Decode(parsedTX.TxData.ActionFields, &createFields)
			parsedTX.TxData.ActionFields = createFields
		case 1:
			var voteFields VoteAction
			mapstructure.Decode(parsedTX.TxData.ActionFields, &voteFields)
			parsedTX.TxData.ActionFields = voteFields
		case 2:
			var voteFields CloseVoting
			mapstructure.Decode(parsedTX.TxData.ActionFields, &voteFields)
			parsedTX.TxData.ActionFields = voteFields
		}

		parsedTX.Transaction.TxData = parsedTX.TxData

		resTX = parsedTX
	case 4:
		parsedTX := new(Notary)
		mapstructure.Decode(txMap, parsedTX)
		parsedTX.Transaction.TxData = parsedTX.TxData

		resTX = parsedTX
	}

	return resTX
}
