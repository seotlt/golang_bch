package helper

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

// Config repesents basic config fields
type Config struct {
	WsPort  int      `json:"wsPort"`
	RPCPort int      `json:"rpcPort"`
	Hosts   []string `json:"hosts"`
}

var (
	configPath = "./config.json"
	config Config
)

func init() {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	configPath = exPath+"/config.json"
}

// GetConfig returns config.json in Config struct
func GetConfig() Config {
	return config
}

// ReadConfig reads and saves config
func ReadConfig() {
	newConfig := new(Config)

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		log.Fatalln("Config does not exist!" + configPath)
	}

	configData, err := ioutil.ReadFile(configPath)
	if err != nil {
		log.Fatalln("Failed to read config.json:", err)
	}

	if err := json.Unmarshal(configData, &newConfig); err != nil {
		log.Fatalln("Failed to unmarshal config:", err)
	}

	config = *newConfig
}
