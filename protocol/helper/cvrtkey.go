package helper

import (
	"crypto/ecdsa"
	"encoding/hex"

	"github.com/ethereum/go-ethereum/crypto"
)

// FromPrivateToPublic translate private key
// as string into public key as string
func FromPrivateToPublic(privateKeyStr string) string {
	privateKey, err := crypto.HexToECDSA(privateKeyStr)
	if err != nil {
		panic(err)
	}

	ecdsaPubKey := privateKey.Public().(*ecdsa.PublicKey)
	bytePubKey := crypto.FromECDSAPub(ecdsaPubKey)

	return hex.EncodeToString(bytePubKey)[2:]
}
