package helper

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"io"
	"log"
	"sort"

	"github.com/cbergoon/merkletree"
)

// MerkleEl represents structure of element of Merkle Tree
type MerkleEl struct {
	Hash string
}

// CalculateHash calculates and returns hash in byte array
func (el MerkleEl) CalculateHash() ([]byte, error) {
	h := sha256.New()
	if _, err := h.Write([]byte(el.Hash)); err != nil {
		return nil, err
	}

	return h.Sum(nil), nil
}

// Equals compares 2 Merkle elements
func (el MerkleEl) Equals(other merkletree.Content) (bool, error) {
	return el.Hash == other.(MerkleEl).Hash, nil
}

// CalcHash calculates hash by input byteStr
func CalcHash(byteStr []byte) string {
	byteHash := sha256.Sum256(byteStr)

	return hex.EncodeToString(byteHash[:])
}

// GenSalt generates random salt
func GenSalt() []byte {
	salt := make([]byte, 8)
	if _, err := io.ReadFull(rand.Reader, salt); err != nil {
		log.Println(err)
	}

	return salt
}

// InsertSort inserts element in slice sorted by hash
func InsertSort(dataMerkle []MerkleEl, data []merkletree.Content, el MerkleEl) ([]MerkleEl, []merkletree.Content) {
	index := sort.Search(len(data), func(i int) bool { return dataMerkle[i].Hash > el.Hash })
	data = append(data, MerkleEl{})
	copy(data[index+1:], data[index:])
	dataMerkle = append(dataMerkle, MerkleEl{})
	copy(dataMerkle[index+1:], dataMerkle[index:])
	data[index] = el
	dataMerkle[index] = el

	return dataMerkle, data
}

// GetMerkleRoot builds Merkle Tree by slice of merkletree.Content and returns
// root of the tree
func GetMerkleRoot(orderedList []merkletree.Content) ([]byte, error) {
	tree, err := merkletree.NewTree(orderedList)
	if err != nil {
		return nil, err
	}

	return tree.MerkleRoot(), nil
}
