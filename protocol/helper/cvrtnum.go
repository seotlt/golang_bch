package helper

import (
	"errors"
	"strconv"
	"strings"
)

// GetOffset returns float num with additional zeros at the end
func GetOffset(num int) float64 {
	decimal := addZeros("1", num)
	floatDecimal, _ := strconv.ParseFloat(decimal, 64)

	return floatDecimal
}

func addZeros(str string, amount int) string {
	str += strings.Repeat("0", amount)
	return str
}

func ConvertFloatToInt(num float64, decimal int) (int, error) {
	numString := strconv.FormatFloat(num, 'f', -1, 64)
	numParts := strings.Split(numString, ".")

	if len(numParts) == 1 {
		resNum, err := strconv.Atoi(addZeros(numParts[0], decimal))
		if err != nil {
			return 0, err
		}

		return resNum, nil
	}

	realPartLen := len(numParts[1])
	if realPartLen > decimal {
		return 0, errors.New("Real part of number more than specified")
	}

	if realPartLen < decimal {
		numParts[1] = addZeros(numParts[1], decimal-realPartLen)
	}

	resNum, err := strconv.Atoi(numParts[0] + numParts[1])
	if err != nil {
		return 0, err
	}

	return resNum, nil
}
