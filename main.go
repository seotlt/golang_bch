package main

import (
	"bytes"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/seotlt/golang_bch/controller"
	"gitlab.com/seotlt/golang_bch/network/node"
	"gitlab.com/seotlt/golang_bch/protocol"
	"gitlab.com/seotlt/golang_bch/protocol/helper"
	"gopkg.in/abiosoft/ishell.v2"
)

func main() {
	protocol.InitLatestBlock()
	helper.ReadConfig()

	// create new shell.
	// by default, new shell includes 'exit', 'help' and 'clear' commands.
	shell := ishell.New()

	// display welcome info.
	shell.Println("Добро пожаловать в консольный интерфес проекта \"Noname\" \nЧтобы увидеть все доступыне комманды, а также подсказки к ним, введите \"help\"\n")

	var addresses []string
	keystores, err := controller.GetAllWallets()
	if err != nil {
		shell.Println(err)
		return
	}

	if len(keystores) == 0 {
		shell.Println("You have no any wallets now.\nPlease create new wallet first.")

		shell.Printf("Enter password: ")
		password := []byte(shell.ReadPassword())
		wallet, err := controller.GetNewWallet(password)
		if err != nil {
			shell.Println(err)
			return
		}

		shell.Printf("Wallet created: \nAddress: %s\nPublic Key: %s\nPrivate Key: %s\nWallet %s has been selected!\n",
			wallet.Address, wallet.PublicKey, wallet.PrivateKey, wallet.Address)
		shell.SetPrompt(wallet.Address[:5] + "... >>> ")
	} else {
		for _, ks := range keystores {
			ksFilename := ks.Name()

			address := "0x" + ksFilename[:len(ksFilename)-5]
			addresses = append(addresses, address)
		}

		selAddrInt := shell.MultiChoice(
			addresses,
			"Select wallet:",
		)
		selWalletFname := keystores[selAddrInt].Name()

		shell.Printf("Enter password: ")
		password := []byte(shell.ReadPassword())

		if err := controller.UnlockWallet(selWalletFname, password); err != nil {
			shell.Println(err)
			return
		}

		shell.Printf("Wallet %s has been selected!\n", addresses[selAddrInt])
		shell.SetPrompt(addresses[selAddrInt][:10] + "... >>> ")
	}

	shell.AddCmd(&ishell.Cmd{
		Name: "start",
		Help: "Запуск ноды",
		Func: func(c *ishell.Context) {
			autoConnect := true
			for _, arg := range c.Args {
				switch arg {
				case "-t":
					autoConnect = false
				case "-h":
					rpcPort := controller.StartRPC()
					c.Printf("RPC сервер запущен на :%s\n", rpcPort)
				}
			}

			ip, wsPort, err := controller.StartNode(autoConnect)
			if err != nil {
				c.Err(err)
				return
			}

			c.Printf("Нода успешно запущена %s:%s\n", ip, wsPort)
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "connect",
		Help: "Подключение к внешней ноде",
		Func: func(c *ishell.Context) {
			if err := controller.Connect(c.Args[0]); err != nil {
				c.Err(err)
				return
			}

			c.Printf("Соединнение с %s установлено\n", strings.Join(c.Args, " "))
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "wallet",
		Help: "Управление кошельками",
		Func: func(c *ishell.Context) {
			action := c.MultiChoice(
				[]string{
					"Create",
					"Import",
					"Select",
					"List",
				},
				"Select one of the following actions:",
			)

			switch action {
			case 0: //Create
				c.Printf("Enter password: ")
				password := []byte(c.ReadPassword())
				wallet, err := controller.GetNewWallet(password)
				if err != nil {
					c.Err(err)
					return
				}

				c.Printf("Wallet created: \nAddress: %s\nPublic Key: %s\nPrivate Key: %s\nWallet %s has been selected!\n",
					wallet.Address, wallet.PublicKey, wallet.PrivateKey, wallet.Address)
				shell.SetPrompt(wallet.Address[:10] + "... >>> ")
			case 1: //Import
				c.Printf("Enter wallet private key: ")
				privKey := c.ReadPassword()
				c.Printf("Enter password: ")
				password := []byte(c.ReadPassword())

				address, err := controller.ImportWallet(privKey, password)
				if err != nil {
					c.Err(err)
					return
				}

				c.Printf("Wallet imported: %s\nWallet %s has been selected!\n", address, address)
				shell.SetPrompt(address[:10] + "... >>> ")
			case 2: //Select
				var addresses []string
				keystores, err := controller.GetAllWallets()
				if err != nil {
					c.Err(err)
					return
				}

				if len(keystores) == 0 {
					c.Println("You have not any wallets now.\nPlease create or import wallet first.")
					return
				}
				for _, ks := range keystores {
					ksFilename := ks.Name()

					address := "0x" + ksFilename[:len(ksFilename)-5]
					addresses = append(addresses, address)
				}

				selAddrInt := c.MultiChoice(
					addresses,
					"Select wallet:",
				)
				selWalletFname := keystores[selAddrInt].Name()

				c.Printf("Enter password: ")
				password := []byte(c.ReadPassword())

				if err := controller.UnlockWallet(selWalletFname, password); err != nil {
					c.Err(err)
					return
				}

				c.Printf("Wallet %s has been selected!\n", addresses[selAddrInt])
				shell.SetPrompt(addresses[selAddrInt][:10] + "... >>> ")
			case 3: //List
				keystores, err := controller.GetAllWallets()
				if err != nil {
					c.Err(err)
					return
				}

				c.Println("Avaliable wallets:")
				for _, ks := range keystores {
					ksFilename := ks.Name()

					address := ksFilename[:len(ksFilename)-5]
					c.Printf("* 0x%s\n", address)
				}

				return
			}
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "forge",
		Help: "Форжинг",
		Func: func(c *ishell.Context) {
			if err := controller.StartForging(); err != nil {
				c.Err(err)
				return
			}

			c.Println("Форжинг запущен")
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "fstop",
		Help: "Остановить форжинг",
		Func: func(c *ishell.Context) {
			controller.StopForging()

			c.Println("Форжинг остановлен")
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "send",
		Help: "Отправка транзакции",
		Func: func(c *ishell.Context) {
			txTypes := controller.GetTXTypes()

			txType := int8(c.MultiChoice(
				txTypes,
				"Select type of transaction you want to send:",
			))

			var recipient string
			optParams := make(map[string]string)

			switch txType {
			case 0:
				c.Printf("Enter address of the recipient: ")
				recipient = c.ReadLine()

				c.Printf("Enter amount of coins you want to send: ")
				amount := c.ReadLine()

				optParams["amount"] = amount
			case 1:
				c.Printf("Enter address of the recipient: ")
				recipient = c.ReadLine()

				c.Printf("Enter amount of coins you want to send: ")
				amount := c.ReadLine()

				optParams["amount"] = amount
			case 2:
				actions := []string{
					"Create",
					"Transfer",
					"Supply",
				}
				actionType := c.MultiChoice(
					actions,
					"Select type of action:",
				)

				c.Printf("Enter token name: ")
				optParams["tkName"] = c.ReadLine()

				switch actionType {
				case 0: //Create
					c.Printf("Enter long unit name: ")
					optParams["unitLong"] = c.ReadLine()

					c.Printf("Enter short unit name (3 symbols): ")
					optParams["unitShort"] = c.ReadLine()

					c.Printf("Enter length of real part (0-9): ")
					optParams["decimal"] = c.ReadLine()

					c.Printf("Enter inital token supply: ")
					optParams["supply"] = c.ReadLine()

					optParams["mintable"] = strconv.Itoa(c.MultiChoice(
						[]string{"No", "Yes"}, "Do you want to make it mintable?",
					))
				case 1: //Transfer
					c.Printf("Enter address of the recipient: ")
					recipient = c.ReadLine()

					c.Printf("Enter amount of tokens you want to send: ")
					optParams["amount"] = c.ReadLine()
				case 2: //Supply
					c.Printf("Enter amount of tokens you want to send: ")
					optParams["amount"] = c.ReadLine()
				}

				optParams["action"] = strconv.Itoa(actionType)
			case 3:
				actions := []string{
					"Create",
					"Vote",
					"Close",
				}
				actionType := c.MultiChoice(
					actions,
					"Select type of action:",
				)

				switch actionType {
				case 0: //Create
					c.Printf("Enter question: ")
					optParams["question"] = c.ReadLine()

					var voters []string
					c.Printf("Specify who can vote in your voting.\n")
					for {
						c.Printf("Enter address (If you want to end leave empty): ")
						voter := c.ReadLine()
						if len(voter) == 0 {
							break
						}

						voters = append(voters, voter)
					}
					optParams["voters"] = strings.Join(voters, " ")
				case 1: //Vote
					c.Printf("Enter hash of the voting: ")
					optParams["questHash"] = c.ReadLine()

					vtInfo, err := controller.GetVotingInfo(optParams["questHash"])
					if err != nil {
						c.Err(errors.New("This voting doesn't exist"))
					}

					c.Printf("Question: %s\nEnter your answer (0 - Yes, 1 - No, 2 - Abstain, Other - Write your own answer): ", vtInfo.Question)
					optParams["answer"] = c.ReadLine()
				case 2: //Close
					c.Printf("Enter hash of the voting: ")
					optParams["questHash"] = c.ReadLine()
				}

				optParams["action"] = strconv.Itoa(actionType)
			case 4:
				var filePaths []string
				c.Printf("Enter path of files you want to certify.\n")
				for {
					c.Printf("Enter file path (If you want to end leave empty): ")
					filePath := c.ReadLine()
					if len(filePath) == 0 {
						break
					}

					filePaths = append(filePaths, filePath)
				}
				optParams["filePaths"] = strings.Join(filePaths, " ")
			}

			hash, err := controller.SendNewTX(txType, recipient, optParams)
			if err != nil {
				c.Err(err)
				return
			}

			c.Println("Transaction " + hash + " successfully has been sent!")
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "notary",
		Help: "Check notary files",
		Func: func(c *ishell.Context) {
			c.Printf("Enter hash of notary contract: ")
			hash := c.ReadLine()

			c.Printf("Enter path to file or to directory with files: ")
			filePaths := c.ReadLine()

			checked, err := controller.CheckNotary(hash, filePaths)
			if err != nil {
				c.Err(err)
				return
			}

			c.Printf("Checked: %t\n", checked)
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "balance",
		Help: "Получение баланса",
		Func: func(c *ishell.Context) {
			c.Printf("Enter address: ")
			address := c.ReadLine()

			c.Printf("Balance of %s: %d\n", address, controller.GetBalance(0, "", address))
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "balanceof",
		Help: "Получение баланса токена",
		Func: func(c *ishell.Context) {
			c.Printf("Enter token name (leave empty for all tokens): ")
			tkName := c.ReadLine()

			c.Printf("Enter address: ")
			address := c.ReadLine()

			if len(tkName) == 0 {
				c.Println("List of your tokens:")
				for tokenName, balance := range controller.GetUserTokens(address) {
					c.Println(tokenName + " - " + balance)
				}

				return
			}

			tkInfo, err := controller.GetTokenInfo(tkName)
			if err != nil {
				c.Err(err)
				return
			}

			balanceStr := strconv.Itoa(controller.GetBalance(2, tkName, address))
			index := len(balanceStr) - tkInfo.Decimal
			balance := balanceStr[:index] + "." + balanceStr[index:]
			c.Printf("Balance of %s: %s %s\n", address, balance, tkInfo.UnitShort)
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "token",
		Help: "Получение информации о токене",
		Func: func(c *ishell.Context) {
			c.Printf("Enter token name: ")
			tkName := c.ReadLine()

			tkInfo, err := controller.GetTokenInfo(tkName)
			if err != nil {
				c.Err(err)
				return
			}

			c.Printf("Token %s info:\nAuthor: %s\nLong unit name: %s\nShort unit name: %s\nInitial supply: %s\nMintable: %t\n",
				tkName,
				tkInfo.From,
				tkInfo.UnitLong,
				tkInfo.UnitShort,
				tkInfo.Supply,
				tkInfo.Mintable == 1,
			)
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "state",
		Help: "Получение state",
		Func: func(c *ishell.Context) {
			c.Printf("Enter address: ")
			address := c.ReadLine()

			c.Printf("State of %s: %d\n", address, controller.GetBalance(1, "", address))
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "peers",
		Help: "Получение статуса пииров",
		Func: func(c *ishell.Context) {
			peers := node.GetAllPeers()
			var peersLog bytes.Buffer
			peersLog.WriteString("Всего соединениий: ")
			peersLog.WriteString(strconv.Itoa(len(peers)))
			peersLog.WriteString("\nIp: ")
			for i := 0; i < len(peers); i++ {
				peersLog.WriteString(peers[i].IP)
				var status string
				if peers[i].Status == 1 {
					status = "connected"
				} else {
					status = "disconnected"
				}
				peersLog.WriteString(", Status: ")
				peersLog.WriteString(status)
				peersLog.WriteString("\n")
			}

			c.Printf(peersLog.String())
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "mempool",
		Help: "Вывод транзакций мемпула \n-c --clear - очистить мемпул",
		Func: func(c *ishell.Context) {
			if len(c.Args) > 0 {
				if c.Args[0] == "-c" ||
					c.Args[0] == "--clear" {
					controller.ClearMempool()

					return
				}
			}

			txs := controller.GetMempool()
			if len(txs) == 0 {
				c.Println("There is no transactions yet")
				return
			}

			var txLog bytes.Buffer
			for _, tx := range txs {
				txStruct := tx.GetStruct()
				txLog.WriteString("Hash: ")
				txLog.WriteString(txStruct.Hash)
				txLog.WriteString("\n")
				txLog.WriteString("From: ")
				txLog.WriteString(txStruct.From)
				txLog.WriteString("\n")
				txLog.WriteString("TxData: ")
				txData := fmt.Sprintf("%+v", txStruct.TxData)
				txLog.WriteString(txData)
				txLog.WriteString("\n")
				txLog.WriteString("PublicKey: ")
				txLog.WriteString(txStruct.PublicKey)
				txLog.WriteString("\n")
				txLog.WriteString("Timestamp: ")
				txLog.WriteString(strconv.Itoa(int(txStruct.Timestamp)))
				txLog.WriteString("\n")
				txLog.WriteString("Signature: ")
				txLog.WriteString(txStruct.Signature)
				txLog.WriteString("\n")
				txLog.WriteString("------\n")

				c.Println(txLog.String())
			}
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "voting",
		Help: "Get information about voting",
		Func: func(c *ishell.Context) {
			c.Printf("Enter voting hash: ")
			vtHash := c.ReadLine()

			actions := []string{
				"Info",
				"Result",
			}

			action := c.MultiChoice(
				actions,
				"Choose action: ",
			)

			switch action {
			case 0:
				vtInfo, err := controller.GetVotingInfo(vtHash)
				if err != nil {
					c.Err(err)
				}

				c.Printf("Voting %s info:\nAuthor: %s\nQuestion: %s\nVoters: %s\nClosed: %t\n",
					vtHash,
					vtInfo.From,
					vtInfo.Question,
					vtInfo.Voters,
					vtInfo.Closed,
				)
			case 1:
				vtResult := controller.GetVotingResult(vtHash)

				c.Printf("Voting %s result:\nYes: %d\nNo: %d\nAbstain: %d\nOther: %d\n",
					vtHash,
					vtResult.Yes,
					vtResult.No,
					vtResult.Abstain,
					vtResult.Other,
				)
			}
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "blocks",
		Help: "Получение информации о блоках",
		Func: func(c *ishell.Context) {
			c.Printf("Всего блоков: %d\n", protocol.GetLatestBlock().Index+1)
			var (
				blocks   []protocol.Block
				blockLog bytes.Buffer
				err      error
			)
			blocks, err = protocol.GetBlocks(0, 0)
			if err != nil {
				c.Err(err)
				return
			}

			i := len(blocks) - 11
			if i < 0 {
				i = 0
			}

			for count := 0; count < 10 && i < len(blocks); i++ {
				blockLog.WriteString("Index: ")
				blockLog.WriteString(strconv.Itoa(blocks[i].Index))
				blockLog.WriteString("\n")
				blockLog.WriteString("PrevHash: ")
				blockLog.WriteString(string(blocks[i].PrevHash))
				blockLog.WriteString("\n")
				blockLog.WriteString("Txs: ")
				blockLog.WriteString(strconv.Itoa(len(blocks[i].Transactions)))
				blockLog.WriteString("\n")
				blockLog.WriteString("Timestamp: ")
				blockLog.WriteString(strconv.Itoa(int(blocks[i].Timestamp)))
				blockLog.WriteString("\n")
				blockLog.WriteString("BaseTarget: ")
				blockLog.WriteString(strconv.Itoa(int(blocks[i].BaseTarget)))
				blockLog.WriteString("\n")
				blockLog.WriteString("GenSignature: ")
				blockLog.WriteString(string(blocks[i].GenSignature))
				blockLog.WriteString("\n")
				blockLog.WriteString("CumulativeDiff: ")
				blockLog.WriteString(blocks[i].CumulativeDiff)
				blockLog.WriteString("\n")
				blockLog.WriteString("Generator: ")
				blockLog.WriteString(string(blocks[i].Generator))
				blockLog.WriteString("\n")
				blockLog.WriteString("PublicKey: ")
				blockLog.WriteString(string(blocks[i].PublicKey))
				blockLog.WriteString("\n")
				blockLog.WriteString("------\n")
			}
			c.Printf("Последние 10 блоков:\n%s", blockLog.String())
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "tx",
		Help: "Получение информации о транзакции",
		Func: func(c *ishell.Context) {
			c.Printf("Enter hash of tx: ")
			hash := c.ReadLine()
			if len(hash) == 0 {
				transactions := controller.GetAllTransactions()
				for _, tx := range transactions {
					var txLog bytes.Buffer
					txStruct := tx.GetStruct()
					txLog.WriteString("Hash: ")
					txLog.WriteString(tx.GetHash())
					txLog.WriteString("\n")
					txLog.WriteString("From: ")
					txLog.WriteString(txStruct.From)
					txLog.WriteString("\n")
					txLog.WriteString("TxData: ")
					txData := fmt.Sprintf("%+v", txStruct.TxData)
					txLog.WriteString(txData)
					txLog.WriteString("\n")
					txLog.WriteString("PublicKey: ")
					txLog.WriteString(txStruct.PublicKey)
					txLog.WriteString("\n")
					txLog.WriteString("Timestamp: ")
					txLog.WriteString(strconv.Itoa(int(txStruct.Timestamp)))
					txLog.WriteString("\n")
					txLog.WriteString("Signature: ")
					txLog.WriteString(txStruct.Signature)
					txLog.WriteString("\n")
					txLog.WriteString("------\n")

					c.Println(txLog.String())
				}

				return
			}
			tx, err := controller.SearchTX(hash)
			if err != nil {
				c.Println("Транзакция с данным хешем не найдена.")
				return
			}
			var txLog bytes.Buffer
			txStruct := tx.GetStruct()
			txLog.WriteString("Hash: ")
			txLog.WriteString(tx.GetHash())
			txLog.WriteString("\n")
			txLog.WriteString("From: ")
			txLog.WriteString(txStruct.From)
			txLog.WriteString("\n")
			txLog.WriteString("TxData: ")
			txData := fmt.Sprintf("%+v", txStruct.TxData)
			txLog.WriteString(txData)
			txLog.WriteString("\n")
			txLog.WriteString("PublicKey: ")
			txLog.WriteString(txStruct.PublicKey)
			txLog.WriteString("\n")
			txLog.WriteString("Timestamp: ")
			txLog.WriteString(strconv.Itoa(int(txStruct.Timestamp)))
			txLog.WriteString("\n")
			txLog.WriteString("Signature: ")
			txLog.WriteString(txStruct.Signature)
			txLog.WriteString("\n")
			txLog.WriteString("------\n")

			c.Println(txLog.String())
		},
	})

	shell.AddCmd(&ishell.Cmd{
		Name: "gen",
		Help: "Генерация тестовых транзакций",
		Func: func(c *ishell.Context) {
			c.Print("Введите кол-во транзакций: ")
			amount, _ := strconv.Atoi(c.ReadLine())

			controller.GenTXs(amount)
			c.Printf("%d транзакций сгенерировано!\n", amount)
		},
	})

	// run shell
	shell.Run()
}
