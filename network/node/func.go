package node

import (
	"encoding/json"
	"log"

	"gitlab.com/seotlt/golang_bch/protocol"
)

// SendBlock makes broadcast of latest block
func SendBlock(block *protocol.Block) {
	msg := SendLatestBlock(*block)
	broadcast(msg)
}

// SendTX makes broadcast of latest block
func SendTX(tx protocol.TransactionInt) {
	msg := SendTXMsg(tx)
	broadcast(msg)
}

func broadcast(message *Message) {
	for _, peer := range PeerList {
		if peer.Status != 1 {
			continue
		}

		if err := Write(peer, message); err != nil {
			log.Println(err)
		}
	}
}

// Write sents message to peer
func Write(peer *Node, message *Message) error {
	msg, _ := json.Marshal(message)
	peer.Mu.Lock()
	defer peer.Mu.Unlock()
	if err := peer.Connection.WriteMessage(1, msg); err != nil {
		return err
	}

	return nil
}
