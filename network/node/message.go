package node

import (
	"encoding/json"

	"gitlab.com/seotlt/golang_bch/protocol"
)

const (
	ReceiveTx                = 0
	RequestLatestBlock       = 1
	ReceiveLatestBlock       = 2
	RequestBlockchain        = 3
	ReceiveBlockchain        = 4
	RequestNodes             = 5
	ReceiveNodes             = 6
	ReceiveReverseConnection = 7
	RequestReverseConnection = 8
	RequestMempool           = 9
	ReceiveMempool           = 10
	ReceiveText              = 11
	RequestPort              = 12
	ReceivePort              = 13
	ReceiveNewBlockchain     = 14
)

// Message - сообщения передаваемые в рамках протокола между узлами.
type Message struct {
	MessageType int             `json:"MessageType"`
	Data        json.RawMessage `json:"data"`
}

type BlocksRequestInfo struct {
	Index     int  `json:"index"`
	Direction bool `json:"direction"` // false - down, true - up
}

func SendText(message string) *Message {
	data, _ := json.Marshal(message)
	return &Message{MessageType: ReceiveText, Data: json.RawMessage(data)}
}

func SendTXMsg(tx protocol.TransactionInt) *Message {
	data, _ := json.Marshal(tx)
	return &Message{MessageType: ReceiveTx, Data: json.RawMessage(data)}
}

func GetLatestBlock() *Message {
	return &Message{MessageType: RequestLatestBlock}
}

func GetBlockchain(requestInfo BlocksRequestInfo) *Message {
	data, _ := json.Marshal(requestInfo)
	return &Message{MessageType: RequestBlockchain, Data: json.RawMessage(data)}
}

func GetNodes() *Message {
	return &Message{MessageType: RequestNodes}
}

func SendNodes(hosts []*Node) *Message {
	data, _ := json.Marshal(hosts)
	return &Message{MessageType: ReceiveNodes, Data: json.RawMessage(data)}
}

func GetMempool() *Message {
	return &Message{MessageType: RequestMempool}
}

func SendMempool(txs []protocol.TransactionInt) *Message {
	data, _ := json.Marshal(txs)
	return &Message{MessageType: ReceiveMempool, Data: json.RawMessage(data)}
}

func GetReverseConnection(ip string) *Message {
	data, _ := json.Marshal(ip)
	return &Message{MessageType: RequestReverseConnection, Data: json.RawMessage(data)}
}

func SendLatestBlock(block protocol.Block) *Message {
	data, _ := json.Marshal(block)
	return &Message{MessageType: ReceiveLatestBlock, Data: json.RawMessage(data)}
}

func SendBlockchain(blocks []protocol.Block) *Message {
	data, _ := json.Marshal(blocks)
	return &Message{MessageType: ReceiveBlockchain, Data: json.RawMessage(data)}
}

func SendNewBlockchain(blocks []protocol.Block) *Message {
	data, _ := json.Marshal(blocks)
	return &Message{MessageType: ReceiveNewBlockchain, Data: json.RawMessage(data)}
}

func GetPort() *Message {
	return &Message{MessageType: RequestPort}
}

func SendPort(port int) *Message {
	data, _ := json.Marshal(port)
	return &Message{MessageType: ReceivePort, Data: json.RawMessage(data)}
}
