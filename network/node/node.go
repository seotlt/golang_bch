package node

import (
	"encoding/json"
	"net"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/seotlt/golang_bch/db"
)

var (
	// PeerList - Список всех узлов
	PeerList    = make(map[string]*Node)
	peerListMut = sync.RWMutex{}
	nodesDB     = db.GetInstance("nodes")
)

// Node - описание узла доутспного для подключения
type Node struct {
	Connection *websocket.Conn `json:"connection"`
	Port       string          `json:"port"`
	IP         string          `json:"ip"`
	Status     int8            `json:"status"`
	Mu         sync.Mutex      `json:"-"`
}

// GetLocalIP - Возвращает ip текущего узла
func GetLocalIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "1"
	}
	for _, address := range addrs {
		// check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return "2"
}

// SetPeer saves peer in DB
func SetPeer(node *Node) {
	data, _ := json.Marshal(node)
	peerListMut.Lock()
	PeerList[node.IP] = node
	peerListMut.Unlock()
	if node.IP != GetLocalIP() {
		nodesDB.Put([]byte(node.IP), data)
	}
}

// GetAllPeers returns all peers from DB
func GetAllPeers() []*Node {
	nodesIter := nodesDB.NewIterator()
	var nodesResp []*Node
	for nodesIter.Next() {
		var nodeItem Node
		json.Unmarshal(nodesIter.Value(), &nodeItem)
		nodeItem.Mu = sync.Mutex{}
		nodesResp = append(nodesResp, &nodeItem)
	}
	return nodesResp
}

// GetPeerByIP returns peer by ip addr of peer
func GetPeerByIP(ip string) *Node {
	if peerL, has := PeerList[ip]; has {
		return peerL
	}

	var peer Node
	peerData, err := nodesDB.Get([]byte(ip))
	if err == nil {
		json.Unmarshal(peerData, &peer)
		peer.Mu = sync.Mutex{}
		return &peer
	}

	return &Node{IP: ip, Port: "0", Status: 3, Mu: sync.Mutex{}}
}

// GetSendList returns slice of peers without
// ip transfered by args
func GetSendList(withOutThisIP string) []*Node {
	var (
		Peer []*Node
	)
	Peer = GetAllPeers()

	var i int
	for i = 0; i < len(Peer); i++ {
		if Peer[i].IP == withOutThisIP {
			Peer = append(Peer[0:i], Peer[i+1:]...)
		}
	}
	return Peer
}
