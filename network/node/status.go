package node

var (
	// Syncing triggers state change to 1 when changed
	Syncing = make(chan bool)
	// Synced triggers state change to 0 when changed
	Synced = make(chan bool)
	// State represents current status of node
	State = 0

	// WaitSynced represents synced status
	WaitSynced chan bool
)

func init() {
	go func() {
		for {
			select {
			case <-Syncing:
				State = 1
			case <-Synced:
				State = 0
				if WaitSynced != nil {
					WaitSynced <- true
					WaitSynced = nil
				}
			}
		}
	}()
}

// WaitSync stop goroutine until node will be in sync
func WaitSync() *chan bool {
	WaitSynced = make(chan bool)

	return &WaitSynced
}
