package network

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/seotlt/golang_bch/protocol/helper"

	"gitlab.com/seotlt/golang_bch/protocol/wallet"

	"github.com/gorilla/websocket"
	"gitlab.com/seotlt/golang_bch/network/node"
	"gitlab.com/seotlt/golang_bch/protocol"
	"gitlab.com/seotlt/golang_bch/protocol/forging"
)

var (
	upgrader      = websocket.Upgrader{}
	networkStatus = false
)

const (
	blocksTransferAmount = 10
)

func handshake(w http.ResponseWriter, r *http.Request) {
	//нужно для тестов на локалке
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	wsServer, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		//event onError
		log.Println("Не удалось сделать upgrade соединения:", err)
		return
	}

	//event onConnection
	log.Println("подключился:", wsServer.RemoteAddr())
	nodeIP := strings.Split(wsServer.RemoteAddr().String(), ":")
	peer := node.GetPeerByIP(nodeIP[0])
	if peer.Status == 3 {
		peer.IP = nodeIP[0]
		peer.Port = nodeIP[1]
		peer.Mu = sync.Mutex{}
	}
	peer.Connection = wsServer
	peer.Status = 1

	if err := initConnection(peer); err != nil {
		log.Println(err)
		return
	}

	go messageListener(wsServer)
}

// GetStatus - состояние ws-сервера (вкл/выкл)
func GetStatus() bool {
	return networkStatus
}

// StartListen - запуск ws сервера
func StartListen(port string, autoConnectToNodes bool, hosts []string) {
	networkStatus = true
	http.HandleFunc("/ws", handshake)

	if autoConnectToNodes {
		nodes := node.GetAllPeers()
		if len(nodes) == 0 {
			var i int
			for i = 0; i < len(hosts); i++ {
				nodes = append(nodes, node.GetPeerByIP(hosts[i]))
			}
		}
		log.Printf("nodes count: %d\n", len(nodes))

		go (func() {
			for i := 0; i < len(nodes); i++ {
				var con, err = Connect(nodes[i])
				nodes[i].Connection = con
				if err != nil {
					nodes[i].Status = 2
					log.Printf("err: %s", err)
				} else {
					nodes[i].Status = 1
					log.Printf("err: %s", err)
				}
				node.SetPeer(nodes[i])
			}
		})()
	}

	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func handleMessage(wsServer *websocket.Conn, mt int, message []byte) {
	log.Printf("recv: %s, %d", string(message), mt)

	remoteAddr := wsServer.RemoteAddr().String()
	nodeIP := strings.Split(remoteAddr, ":")
	peer := node.GetPeerByIP(nodeIP[0])
	peer.Connection = wsServer
	if peer.Status == 3 {
		peer.IP = nodeIP[0]
		peer.Port = nodeIP[1]
		peer.Mu = sync.Mutex{}
	}
	peer.Status = 1

	msg := node.Message{}
	err := json.Unmarshal(message, &msg)
	if err != nil {
		log.Printf("%s", err)

		return
	}

	switch msg.MessageType {
	case node.RequestPort:
		log.Println(`отправляю порт`)
		config := helper.GetConfig()
		node.Write(peer, node.SendPort(config.WsPort))
	case node.ReceivePort:
		log.Println(`получил порт`)
		var port int
		json.Unmarshal(msg.Data, &port)
		peer.Port = strconv.Itoa(port)

		node.SetPeer(peer)
	case node.ReceiveTx:
		log.Printf(`получил транзакцию`)
		tx, err := protocol.JSONToTransaction(msg.Data)
		if err != nil {
			log.Fatalf(`Не удалось распарсить транзакцию: %s`, err)
			return
		}

		go handleTX(tx)
	case node.RequestLatestBlock:
		log.Printf(`отправляю последний блок`)
		node.Write(peer, node.SendLatestBlock(protocol.GetLatestBlock()))
	case node.ReceiveLatestBlock:
		log.Printf(`получил последний блок`)
		var block protocol.Block
		err = json.Unmarshal(msg.Data, &block)
		if err != nil {
			log.Fatalf(`Не удалось распарсить блок: %s`, err)
			return
		}

		go handleReceivedBlock(block, peer)
	case node.RequestBlockchain:
		log.Printf(`отправляю блокчейн`)
		var requestInfo node.BlocksRequestInfo
		err := json.Unmarshal([]byte(msg.Data), &requestInfo)
		if err != nil {
			log.Println("Неверный запрос на отправку блокчейна")
			return
		}

		index := requestInfo.Index
		var limit int
		if requestInfo.Index == 0 && !requestInfo.Direction {
			latestBlockIndex := protocol.GetLatestBlock().Index
			index = latestBlockIndex - blocksTransferAmount

			limit = latestBlockIndex + 1
		} else if requestInfo.Direction {
			limit = index + blocksTransferAmount
		} else {
			limit = index + 1
			index = index - blocksTransferAmount
		}
		if index < 0 {
			index = 0
		}

		blockchain, err := protocol.GetBlocks(index, limit)
		if err != nil {
			log.Println("Не удалось отправить блокчейн:", err)
			return
		}

		if len(blockchain) == 0 {
			return
		}

		if requestInfo.Direction {
			node.Write(peer, node.SendNewBlockchain(blockchain))
			return
		}
		node.Write(peer, node.SendBlockchain(blockchain))
	case node.ReceiveBlockchain:
		log.Printf(`получил блокчейн`)
		var blocks []protocol.Block
		err := json.Unmarshal([]byte(msg.Data), &blocks)
		if err != nil {
			log.Fatalf(`Пришел невалидный блокчейн: %s`, err)
			return
		}

		go handleReceivedBlockchain(blocks, peer)
	case node.ReceiveNewBlockchain:
		log.Printf(`получил новый блокчейн`)
		var blocks []protocol.Block
		err := json.Unmarshal([]byte(msg.Data), &blocks)
		if err != nil {
			log.Fatalf(`Пришел невалидный блокчейн: %s`, err)
			return
		}

		go handleNewChain(blocks, peer)
	case node.RequestNodes:
		log.Printf(`отправляю узлы`)
		node.Write(peer, node.SendNodes(node.GetSendList(peer.IP)))
	case node.ReceiveNodes:
		log.Printf(`получил узлы`)
		var nodes []node.Node
		err := json.Unmarshal([]byte(msg.Data), &nodes)
		if err != nil {
			log.Fatalf(`Пришли невалидные узлы: %s`, err)
			return
		}

		go handleReceivedHosts(nodes)
	case node.RequestMempool:
		log.Printf(`отправляю мемпул`)
		mempool, _ := protocol.GetMempoolTxs()
		node.Write(peer, node.SendMempool(mempool))
	case node.ReceiveMempool:
		log.Printf(`получил мемпул`)
		var (
			txsJSON []json.RawMessage
			txs     []protocol.TransactionInt
		)

		err := json.Unmarshal([]byte(msg.Data), &txsJSON)
		if err != nil {
			log.Fatalf("Не удалось распарсить мемпул: %s", err)
		}

		for _, txJSON := range txsJSON {
			tx, err := protocol.JSONToTransaction(txJSON)
			if err != nil {
				log.Println(err)
			}

			txs = append(txs, tx)
		}

		handleReceivedMempool(txs)
	case node.ReceiveText:
		log.Printf(`получил текст`)
	}
}

func initConnection(peer *node.Node) error {
	if err := node.Write(peer, node.GetPort()); err != nil {
		return err
	}

	if err := node.Write(peer, node.GetNodes()); err != nil {
		return err
	}
	if err := node.Write(peer, node.GetLatestBlock()); err != nil {
		return err
	}
	if err := node.Write(peer, node.GetMempool()); err != nil {
		return err
	}

	return nil
}

func handleTX(tx protocol.TransactionInt) {
	if protocol.AlreadyInMempool(tx.GetHash()) {
		return
	}

	if err := protocol.CheckTransaction(tx, false); err != nil {
		log.Println(err)
		return
	}
	protocol.AddToMempool(tx)

	node.SendTX(tx)
}

func handleReceivedBlock(receivedBlock protocol.Block, peer *node.Node) {
	latestBlock := protocol.GetLatestBlock()
	if receivedBlock.Index <= latestBlock.Index {
		return
	}

	if node.State == 1 {
		log.Println("Wait sync in receive latest block")
		<-*node.WaitSync()
		log.Println("synced")
	}

	node.Syncing <- true

	if latestBlock.Hash == receivedBlock.PrevHash {
		if err := protocol.IsValidNextBlock(&receivedBlock, &latestBlock); err != nil {
			log.Println(err)
			node.Synced <- true
			return
		}

		if err := receivedBlock.SaveBlock(); err != nil {
			log.Println(err)
			node.Synced <- true
			return
		}
		protocol.SetLatestBlock(receivedBlock)
		go protocol.SaveTXs(receivedBlock.Transactions)

		node.SendBlock(&receivedBlock)

		forging.StartForging(wallet.Wallet{})

		node.Synced <- true
		return
	}

	node.Synced <- true

	if receivedBlock.Index > latestBlock.Index {
		requestInfo := node.BlocksRequestInfo{
			Index:     latestBlock.Index,
			Direction: false,
		}
		node.Write(peer, node.GetBlockchain(requestInfo))
	}
}

func handleNewChain(receivedChain []protocol.Block, peer *node.Node) {
	node.Syncing <- true

	if err := addBlockDiff(receivedChain); err != nil {
		log.Println("handleNewChain error: " + err.Error())

		node.Synced <- true
		return
	}

	requestInfo := node.BlocksRequestInfo{
		Index:     receivedChain[len(receivedChain)-1].Index + 1,
		Direction: true,
	}
	node.Write(peer, node.GetBlockchain(requestInfo))

	node.Synced <- true
}

func handleReceivedBlockchain(receivedChain []protocol.Block, peer *node.Node) {
	node.Syncing <- true
	chainLen := len(receivedChain)
	ownChain, err := protocol.GetBlocks(receivedChain[0].Index, receivedChain[chainLen-1].Index+1)
	if err != nil {
		log.Println("handleReceivedBlockchain:", err)
		node.Synced <- true
		return
	}

	commonBlockInd := compareChains(receivedChain, ownChain)
	var requestInfo node.BlocksRequestInfo
	if commonBlockInd == 0 && receivedChain[0].Index != 0 {
		requestInfo = node.BlocksRequestInfo{
			Index:     receivedChain[0].Index,
			Direction: false,
		}

		node.Write(peer, node.GetBlockchain(requestInfo))
		node.Synced <- true

		return
	}

	requestInfo = node.BlocksRequestInfo{
		Index:     commonBlockInd + 1,
		Direction: true,
	}
	node.Write(peer, node.GetBlockchain(requestInfo))

	node.Synced <- true
}

func compareChains(receivedChain, ownChain []protocol.Block) int {
	var commonBlockInd int
	for i := len(ownChain) - 1; i > 0; i-- {
		if receivedChain[i].Hash == ownChain[i].Hash {
			commonBlockInd = receivedChain[i].Index

			return commonBlockInd
		}

		protocol.DeleteBlock(ownChain[i].Hash, ownChain[i].Index)
		protocol.SetLatestBlock(ownChain[i-1])
	}

	return 0
}

func addBlockDiff(diff []protocol.Block) error {
	for _, block := range diff {
		latestBlock := protocol.GetLatestBlock()
		if err := protocol.IsValidNextBlock(&block, &latestBlock); err != nil {
			return err
		}

		if err := block.SaveBlock(); err != nil {
			return err
		}
		protocol.SetLatestBlock(block)
		go protocol.SaveTXs(block.Transactions)

		node.SendBlock(&block)

		forging.StartForging(wallet.Wallet{})
	}

	return nil
}

func handleReceivedMempool(txs []protocol.TransactionInt) {
	for _, tx := range txs {
		if err := protocol.CheckTransaction(tx, false); err != nil {
			log.Println(err)
			continue
		}
		protocol.AddToMempool(tx)
	}
}

// Connect establishes connection between two nodes
func Connect(peer *node.Node) (*websocket.Conn, error) {
	header := http.Header{}
	dialer := websocket.DefaultDialer
	dialer.HandshakeTimeout = 15 * time.Second
	ws, res, err := websocket.DefaultDialer.Dial(fmt.Sprintf("ws://%s/ws", peer.IP+":"+peer.Port), header)
	if err != nil {
		return nil, err
	}
	log.Println("Connect res: ", res)

	peer.Connection = ws
	peer.Status = 1
	peer.Mu = sync.Mutex{}
	node.SetPeer(peer)

	if err := initConnection(peer); err != nil {
		return nil, err
	}

	go messageListener(ws)

	return ws, nil
}

func messageListener(ws *websocket.Conn) {
	defer ws.Close()

	for {
		mt, message, err := ws.ReadMessage()
		if err != nil {
			remoteAddr := ws.RemoteAddr().String()
			nodeIP := strings.Split(remoteAddr, ":")
			peer := node.GetPeerByIP(nodeIP[0])

			peer.Status = 2

			node.SetPeer(peer)
			log.Println("отключился:", err)
			break
		}
		//event onMessage
		handleMessage(ws, mt, message)
	}
}

func handleReceivedHosts(nodes []node.Node) {
	for i := 0; i < len(nodes); i++ {
		var peer = node.GetPeerByIP(nodes[i].IP)
		if peer.Status == 3 {
			conn, err := Connect(peer)
			peer.Connection = conn
			if err != nil {
				peer.Status = 2
			} else {
				peer.Status = 1
			}
			node.SetPeer(peer)
		}
	}
}
