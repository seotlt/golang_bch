package network

import (
	"encoding/json"
	"io/ioutil"
	"log"      // пакет для логирования
	"net/http" // пакет для поддержки HTTP протокола
	"strconv"
	"strings"

	"gitlab.com/seotlt/golang_bch/protocol/wallet"

	"gitlab.com/seotlt/golang_bch/network/node"
	"gitlab.com/seotlt/golang_bch/protocol"
)

func genTX(w http.ResponseWriter, r *http.Request) {
	type BodyF struct {
		TxType     int8              `json:"type"`
		FromAddr   string            `json:"fromAddr"`
		FromPubKey string            `json:"fromPubKey"`
		To         string            `json:"to"`
		OptParams  map[string]string `json:"optParams"`
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		httpError(w, "can't read body")
		return
	}

	if len(body) == 0 {
		httpResponser(w, []byte("ok"))
		return
	}

	var fields BodyF
	json.Unmarshal(body, &fields)

	from := wallet.Wallet{
		Address:   fields.FromAddr,
		PublicKey: fields.FromPubKey,
	}
	var transaction protocol.TransactionInt
	switch fields.TxType {
	case 0: //Coin
		transaction, err = protocol.GenCoin(from, fields.To, fields.TxType, fields.OptParams)
	case 1: //State
		transaction, err = protocol.GenCoin(from, fields.To, fields.TxType, fields.OptParams)
	case 2: //Token
		transaction, err = protocol.GenToken(from, fields.To, fields.TxType, fields.OptParams)
	case 3: //Voting
		transaction, err = protocol.GenVoting(from, fields.TxType, fields.OptParams)
	case 4: //Notary
		transaction, err = protocol.GenNotary(from, fields.TxType, fields.OptParams)
	default:
		httpError(w, "Transaction of this type doesn't exist")
	}

	if err != nil {
		httpError(w, err.Error())
	}

	jTX, err := json.Marshal(transaction)
	if err != nil {
		httpError(w, "Could not marshal tx")

		return
	}

	httpResponser(w, jTX)
}

func getBalance(w http.ResponseWriter, r *http.Request) {
	address := strings.Replace(r.URL.Path, "/balance/", "", 1)
	jBalance, _ := json.Marshal(protocol.GetBalance(0, "", address))
	httpResponser(w, jBalance)
}

func getState(w http.ResponseWriter, r *http.Request) {
	address := strings.Replace(r.URL.Path, "/state/", "", 1)
	jState, _ := json.Marshal(protocol.GetBalance(1, "", address))
	httpResponser(w, jState)
}

func getTokenInfo(w http.ResponseWriter, r *http.Request) {
	token := strings.Replace(r.URL.Path, "/token/", "", 1)
	tkParts := strings.Split(token, "/")

	switch len(tkParts) {
	case 1:
		tkInfo, err := protocol.GetTokenInfo(tkParts[0])
		if err != nil {
			httpError(w, err.Error())
			return
		}

		jInfo, _ := json.Marshal(tkInfo)
		httpResponser(w, jInfo)
	case 2:
		tkBalance := protocol.GetBalance(2, tkParts[0], tkParts[1])

		jInfo, _ := json.Marshal(tkBalance)
		httpResponser(w, jInfo)
	}
}

func getVotingInfo(w http.ResponseWriter, r *http.Request) {
	voting := strings.Replace(r.URL.Path, "/voting/", "", 1)
	vtParts := strings.Split(voting, "/")

	switch len(vtParts) {
	case 1:
		vtInfo, err := protocol.GetVotingInfo(vtParts[0])
		if err != nil {
			httpError(w, err.Error())
			return
		}

		jInfo, _ := json.Marshal(vtInfo)
		httpResponser(w, jInfo)
	case 2:
		if vtParts[1] != "result" {
			httpError(w, "Unknown info action")
			return
		}
		vtRes := protocol.GetVotingResult(vtParts[0])

		jInfo, _ := json.Marshal(vtRes)
		httpResponser(w, jInfo)
	}
}

func getPeers(w http.ResponseWriter, r *http.Request) {
	peers := node.GetAllPeers()
	var peersInfo []map[string]string
	for _, peer := range peers {
		peerInfo := make(map[string]string)
		peerInfo["ip"] = peer.IP
		peerInfo["status"] = strconv.Itoa(int(peer.Status))

		peersInfo = append(peersInfo, peerInfo)
	}

	jPeers, _ := json.Marshal(node.GetAllPeers())
	httpResponser(w, jPeers)
}

func getInfo(w http.ResponseWriter, r *http.Request) {
	var info = make(map[string]protocol.Block)
	info["genesis"] = protocol.GetGenesisBlock()
	jInfo, _ := json.Marshal(info)
	httpResponser(w, jInfo)
}

func sendTx(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error reading body: %v", err)
		httpError(w, "can't read body")
		return
	}

	if len(body) == 0 {
		httpResponser(w, []byte("body empty"))
		return
	}

	tx, err := protocol.JSONToTransaction(body)
	if err != nil {
		httpError(w, "Could not to parse tx: "+err.Error())

		return
	}

	if err := protocol.CheckTransaction(tx, false); err != nil {
		httpError(w, "Broken transaction"+err.Error())
		return
	}
	protocol.AddToMempool(tx)

	httpResponser(w, []byte("tx sended"))
}

func getMempoolTx(w http.ResponseWriter, r *http.Request) {
	hash := strings.Replace(r.URL.Path, "/mempool/", "", 1)
	jTx, _ := json.Marshal(protocol.GetMempoolTx(hash))
	httpResponser(w, jTx)
}

func getMempoolTxs(w http.ResponseWriter, r *http.Request) {
	txs, _ := protocol.GetMempoolTxs()
	var empty []protocol.TransactionInt
	if txs == nil {
		txs = empty
	}

	log.Println(txs)
	jTxs, _ := json.Marshal(txs)
	log.Println(string(jTxs))
	httpResponser(w, jTxs)
}

func getTx(w http.ResponseWriter, r *http.Request) {
	hash := strings.Replace(r.URL.Path, "/transactions/", "", 1)
	log.Println(hash)
	tx, err := protocol.GetTxByHash(hash)

	if err != nil {
		http.Error(w, "Don't have tx with this hash", http.StatusForbidden)
	} else {
		jTx, _ := json.Marshal(tx)
		httpResponser(w, jTx)
	}
}

func getTxs(w http.ResponseWriter, r *http.Request) {
	jTxs, _ := json.Marshal(protocol.GetAllTxs())
	httpResponser(w, jTxs)
}

func getBlocks(w http.ResponseWriter, r *http.Request) {
	blockchain, err := protocol.GetBlocks(0, 0)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	jBlocks, _ := json.Marshal(blockchain)
	httpResponser(w, jBlocks)
}

func getPartOfBlocks(w http.ResponseWriter, r *http.Request) {
	limitsStr := strings.Replace(r.URL.Path, "/partblocks/", "", 1)
	limitsSlc := strings.Split(limitsStr, "/")
	if len(limitsSlc) != 2 {
		http.Error(w, "Invalud params", http.StatusBadRequest)
		return
	}

	start, err := strconv.Atoi(limitsSlc[0])
	limit, err := strconv.Atoi(limitsSlc[1])
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	blockchain, err := protocol.GetBlocks(start, limit)
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	jBlocks, _ := json.Marshal(blockchain)
	httpResponser(w, jBlocks)
}

func getBlock(w http.ResponseWriter, r *http.Request) {
	hash := strings.Replace(r.URL.Path, "/blocks/", "", 1)
	if _, err := strconv.Atoi(hash); err == nil {
		_block, _err := protocol.GetBlockByIndex(hash)
		if _err != nil {
			http.Error(w, "Don't have block with this index", http.StatusForbidden)
		} else {
			jBlock, _ := json.Marshal(_block)
			httpResponser(w, jBlock)
		}
	} else {
		block, err := protocol.GetBlockByHash(hash)
		log.Println(err)
		if err != nil {
			http.Error(w, "Don't have block with this hash", http.StatusForbidden)
		} else {
			jBlock, _ := json.Marshal(block)
			httpResponser(w, jBlock)
		}
	}
}

func getBlockHashByIndex(w http.ResponseWriter, r *http.Request) {
	index := strings.Replace(r.URL.Path, "/blockhashes/", "", 1)
	block, err := protocol.GetBlockByIndex(index)
	if err != nil {
		http.Error(w, "Don't have block with this index", http.StatusForbidden)
	} else {
		jBlockHash, _ := json.Marshal(block.Hash)
		httpResponser(w, jBlockHash)
	}
}

func getBlockHashes(w http.ResponseWriter, r *http.Request) {
	hashes := protocol.GetAllBlockHashes()
	jHashes, _ := json.Marshal(hashes)
	httpResponser(w, jHashes)
}

func getBlocksLength(w http.ResponseWriter, r *http.Request) {
	length := protocol.GetLatestBlock()
	jLength, _ := json.Marshal(length.Index + 1)
	httpResponser(w, jLength)
}

func searchTx(w http.ResponseWriter, r *http.Request) {
	hash := strings.Replace(r.URL.Path, "/search/", "", 1)
	log.Println(hash)

	if _, err := strconv.Atoi(hash); err == nil {
		_block, _err := protocol.GetBlockByIndex(hash)
		log.Println(_block)
		if _err != nil {
			http.Error(w, "Don't have block with this index", http.StatusForbidden)
		} else {
			jBlock, _ := json.Marshal(_block)
			httpResponser(w, jBlock)
		}
	} else {
		block, err := protocol.GetBlockByHash(hash)
		log.Println(err)
		if err != nil {
			tx, err := protocol.GetTxByHash(hash)
			if err != nil {
				http.Error(w, "Don't have any with this hash", http.StatusForbidden)
			} else {
				jTx, _ := json.Marshal(tx)
				httpResponser(w, jTx)
			}
		} else {
			jBlock, _ := json.Marshal(block)
			httpResponser(w, jBlock)
		}
	}
}

func httpResponser(w http.ResponseWriter, msg []byte) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, X-Requested-With")
	w.Header().Set("Content-Type", "application/json")
	w.Write(msg)
}

func httpError(w http.ResponseWriter, errorMsg string) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, X-Requested-With")
	w.Header().Set("Content-Type", "application/json")
	http.Error(w, errorMsg, 403)
}

// StartRPC starts rpc server
func StartRPC(port string) {
	//routing
	http.HandleFunc("/info", getInfo)
	http.HandleFunc("/balance/", getBalance)
	http.HandleFunc("/state/", getState)
	http.HandleFunc("/peers", getPeers)
	http.HandleFunc("/search/", searchTx) //POST
	http.HandleFunc("/sendtx", sendTx)    //POST
	http.HandleFunc("/gentx", genTX)      //POST
	http.HandleFunc("/mempool/", getMempoolTx)
	http.HandleFunc("/mempool", getMempoolTxs)
	http.HandleFunc("/transactions/", getTx)
	http.HandleFunc("/transactions", getTxs)
	http.HandleFunc("/blocks", getBlocks)
	http.HandleFunc("/blocks/", getBlock)
	http.HandleFunc("/partblocks/", getPartOfBlocks)
	http.HandleFunc("/blockhashes/", getBlockHashByIndex)
	http.HandleFunc("/blockhashes", getBlockHashes)
	http.HandleFunc("/blocks/length", getBlocksLength)
	http.HandleFunc("/token/", getTokenInfo)
	http.HandleFunc("/voting/", getVotingInfo)

	if err := http.ListenAndServe(":"+port, nil); err != nil {
		log.Println(err)
	}
}
