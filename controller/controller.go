package controller

import (
	"errors"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/seotlt/golang_bch/network"
	"gitlab.com/seotlt/golang_bch/network/node"
	"gitlab.com/seotlt/golang_bch/protocol"
	"gitlab.com/seotlt/golang_bch/protocol/forging"
	"gitlab.com/seotlt/golang_bch/protocol/helper"
	"gitlab.com/seotlt/golang_bch/protocol/wallet"
)

func StartNode(autoConnect bool) (string, string, error) {
	config := helper.GetConfig()
	port := strconv.Itoa(config.WsPort)
	hosts := config.Hosts
	ip := node.GetLocalIP()

	if network.GetStatus() {
		return "", "", errors.New("Нода уже запущена")
	}

	go network.StartListen(port, autoConnect, hosts)

	return ip, port, nil
}

func StartRPC() string {
	config := helper.GetConfig()
	port := strconv.Itoa(config.RPCPort)

	go network.StartRPC(port)

	return port
}

func Connect(address string) error {
	nodeIP := strings.Split(address, ":")
	node := node.Node{IP: nodeIP[0], Port: nodeIP[1]}

	if _, err := network.Connect(&node); err != nil {
		return err
	}

	return nil
}

func GetPeers() []map[string]string {
	peers := node.GetAllPeers()
	var peersInfo []map[string]string
	for _, peer := range peers {
		peerInfo := make(map[string]string)
		peerInfo["ip"] = peer.IP
		peerInfo["status"] = strconv.Itoa(int(peer.Status))

		peersInfo = append(peersInfo, peerInfo)
	}

	return peersInfo
}

func StartForging() error {
	currWallet, err := wallet.GetCurrentWallet()
	if err != nil {
		return err
	}

	return forging.StartForging(currWallet)
}

func StopForging() {
	forging.StopForging()
}

func GetForgingState() bool {
	return forging.Forging
}

func GetForgeTime() int {
	return forging.GetForgeTime()
}

func GetNewWallet(password []byte) (wallet.Wallet, error) {
	wl, err := wallet.CreateWallet(password, "")
	if err != nil {
		return wallet.Wallet{}, err
	}

	return *wl, nil
}

func ImportWallet(privKey string, password []byte) (string, error) {
	if len(privKey) == 66 {
		privKey = privKey[2:]
	}
	wl, err := wallet.CreateWallet(password, privKey)
	if err != nil {
		return "", err
	}

	return wl.Address, nil
}

func SendNewTX(txType int8, to string, optParams map[string]string) (string, error) {
	from, err := wallet.GetCurrentWallet()
	if err != nil {
		return "", err
	}

	var transaction protocol.TransactionInt
	switch txType {
	case 0: //Coin
		transaction, err = protocol.GenCoin(from, to, txType, optParams)
	case 1: //State
		transaction, err = protocol.GenCoin(from, to, txType, optParams)
	case 2: //Token
		transaction, err = protocol.GenToken(from, to, txType, optParams)
	case 3: //Voting
		transaction, err = protocol.GenVoting(from, txType, optParams)
	case 4: //Notary
		transaction, err = protocol.GenNotary(from, txType, optParams)
	default:
		return "", errors.New("Transaction of this type doesn't exist")
	}

	if err != nil {
		return "", err
	}

	if err := transaction.SignTX(from.PrivateKey); err != nil {
		return "", err
	}

	if err := protocol.CheckTransaction(transaction, false); err != nil {
		return "", err
	}
	protocol.AddToMempool(transaction)

	node.SendTX(transaction)

	return transaction.GetHash(), nil
}

func GetUserTokens(address string) map[string]string {
	return protocol.GetUserTokens(address)
}

func GetTokenInfo(tkName string) (protocol.TokenInfo, error) {
	tkInfo, err := protocol.GetTokenInfo(tkName)
	if err != nil {
		return protocol.TokenInfo{}, err
	}

	return tkInfo, nil
}

func GetVotingInfo(vtHash string) (protocol.VotingInfo, error) {
	vtInfo, err := protocol.GetVotingInfo(vtHash)
	if err != nil {
		return protocol.VotingInfo{}, err
	}

	return vtInfo, nil
}

func GetVotingResult(vtHash string) protocol.VotingResult {
	return protocol.GetVotingResult(vtHash)
}

func GetBalance(txType int8, tkName string, address string) int {
	return protocol.GetBalance(txType, tkName, address)
}

func GetTXTypes() []string {
	return protocol.GetTXTypes()
}

func GetAllWallets() ([]os.FileInfo, error) {
	keystores, err := wallet.GetAllWalletFiles()
	if err != nil {
		return nil, err
	}

	return keystores, nil
}

func UnlockWallet(ksFilename string, password []byte) error {
	if err := wallet.UnlockWallet(ksFilename, password); err != nil {
		return err
	}

	return nil
}

func GetMempool() []protocol.TransactionInt {
	txs, _ := protocol.GetMempoolTxs()

	return txs
}

func ClearMempool() {
	protocol.ClearMempool()
}

func SearchTX(hash string) (protocol.TransactionInt, error) {
	return protocol.GetTxByHash(hash)
}

func GetAllTransactions() []protocol.TransactionInt {
	return protocol.GetAllTxs()
}

func CheckNotary(hash, filePaths string) (bool, error) {
	return protocol.CheckNotary(hash, filePaths)
}

func DeleteWallet(fname string) {
	if err := wallet.DeleteWallet(fname); err != nil {
		log.Println(err)
	}
}

func GenTXs(count int) {
	start := time.Now()
	for i := 0; i < count; i++ {
		// to, _ := wallet.GenWallet("")
		params := make(map[string]string)
		params["amount"] = "1"

		// sendNewTX(1, to.Address, params)
	}
	log.Printf("Время генерации транзакций %fс", time.Since(start).Seconds())
}
