package db

import (
	"os"
	"path/filepath"
	"sync"

	"github.com/syndtr/goleveldb/leveldb/errors"
	"github.com/syndtr/goleveldb/leveldb/filter"
	"github.com/syndtr/goleveldb/leveldb/iterator"
	"github.com/syndtr/goleveldb/leveldb/util"

	"github.com/syndtr/goleveldb/leveldb/opt"

	"github.com/syndtr/goleveldb/leveldb"
)

var (
	dbPath = "./.store"
)

func init() {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath := filepath.Dir(ex)
	dbPath = exPath+"/.store"
}


// DB is a struct keeping data for a specific db
type DB struct {
	fn  string
	ldb *leveldb.DB

	once sync.Once
}

var instances = make(map[string]*DB)

// GetInstance is a func that create new instance
// of specific db if it's not created before
func GetInstance(fn string) *DB {
	if instances[fn] == nil {
		instances[fn] = new(DB)
	}

	instances[fn].once.Do(func() {
		instances[fn].initDB(fn)
	})

	return instances[fn]
}

func (db *DB) initDB(fn string) *DB {
	path := dbPath + "/" + fn

	var ldb *leveldb.DB
	var err error
	if fn == "blockHashes" {
		ldb, err = leveldb.OpenFile(path, &opt.Options{
			Filter:   filter.NewBloomFilter(10),
			Comparer: indexComparer{}, // for blockHashes db using special comparer
		})
	} else {
		ldb, err = leveldb.OpenFile(path, &opt.Options{
			Filter: filter.NewBloomFilter(10),
		})
	}

	if _, corrupted := err.(*errors.ErrCorrupted); corrupted {
		ldb, err = leveldb.RecoverFile(path, nil)
	}
	if err != nil {
		panic(err)
	}

	db.fn = fn
	db.ldb = ldb

	return db
}

// Get is a func for getting value in db by key
func (db *DB) Get(key []byte) ([]byte, error) {
	val, err := db.ldb.Get(key, nil)
	if err != nil {
		return nil, errors.New(db.fn + " " + err.Error())
	}

	return val, nil
}

// Put is a func to save value in db under specific key
func (db *DB) Put(key, value []byte) error {
	return db.ldb.Put(key, value, nil)
}

// Delete is a func for deleting pair from db
func (db *DB) Delete(key []byte) error {
	return db.ldb.Delete(key, nil)
}

// Has checking is key existing in db or not
func (db *DB) Has(key []byte) (bool, error) {
	return db.ldb.Has(key, nil)
}

// NewIterator creates new iterator without prefix
func (db *DB) NewIterator() iterator.Iterator {
	return db.NewIteratorWithPrefix(nil)
}

// NewIteratorWithPrefix creates new iterator with prefix
func (db *DB) NewIteratorWithPrefix(prefix []byte) iterator.Iterator {
	return db.ldb.NewIterator(util.BytesPrefix(prefix), nil)
}

func (db *DB) NewIteratorWithRange(start []byte, limit []byte) iterator.Iterator {
	return db.ldb.NewIterator(&util.Range{Start: start, Limit: limit}, nil)
}
