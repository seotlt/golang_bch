package db

import (
	"fmt"
	"strconv"
	"testing"
)

func TestDBSingletone(t *testing.T) {
	testDB := GetInstance("test")
	fmt.Println(testDB)
}

func TestDBMethods(t *testing.T) {
	testDB := GetInstance("test")

	key := []byte("testkey")
	value := []byte("randomdata")
	err := testDB.Put(key, value)
	if err != nil {
		panic(err)
	}

	val, err := testDB.Get(key)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s \n", val)

	iter := testDB.NewIterator()
	for iter.Next() {
		fmt.Printf("key: %s, val: %s \n", iter.Key(), iter.Value())
	}

	err = testDB.Delete(key)
	if err != nil {
		panic(err)
	}

	fmt.Println(testDB.Has(key))
}

func TestIndexComparer(t *testing.T) {
	testDB := GetInstance("blockHashes")

	for i := 0; i < 100; i++ {
		key := []byte(strconv.Itoa(i))
		value := []byte("randomdata")

		err := testDB.Put(key, value)
		if err != nil {
			panic(err)
		}
	}

	iter := testDB.NewIterator()
	for iter.Next() {
		fmt.Printf("key: %s, val: %s \n", iter.Key(), iter.Value())
	}
}

func TestGettingRange(t *testing.T) {
	testDB := GetInstance("blockHashes")

	iter := testDB.NewIteratorWithRange([]byte("100"), []byte("110"))
	for iter.Next() {
		fmt.Printf("key: %s, val: %s \n", iter.Key(), iter.Value())
	}
}
