package db

import (
	"strconv"
)

type indexComparer struct{}

func (indexComparer) Compare(a, b []byte) int {
	keyAStr := string(a[:])
	keyBStr := string(b[:])

	if keyAStr == ":" || keyBStr == ":" {
		return 0
	}

	keyA, err := strconv.Atoi(keyAStr)
	keyB, err := strconv.Atoi(keyBStr)
	if err != nil {
		panic(err)
	}

	if keyA > keyB {
		return 1
	}

	if keyA < keyB {
		return -1
	}

	return 0
}

func (indexComparer) Name() string {
	return "indexComparer"
}

func (indexComparer) Separator(dst, a, b []byte) []byte {
	i, n := 0, len(a)
	if n > len(b) {
		n = len(b)
	}
	for ; i < n && a[i] == b[i]; i++ {
	}
	if i >= n {
		// Do not shorten if one string is a prefix of the other
	} else if c := a[i]; c < 0xff && c+1 < b[i] {
		dst = append(dst, a[:i+1]...)
		dst[len(dst)-1]++
		return dst
	}
	return nil
}

func (indexComparer) Successor(dst, b []byte) []byte {
	for i, c := range b {
		if c != 0xff {
			dst = append(dst, b[:i+1]...)
			dst[len(dst)-1]++
			return dst
		}
	}
	return nil
}
