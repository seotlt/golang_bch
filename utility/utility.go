package main

import (
	"encoding/json"
	"os"
	"strconv"

	"gitlab.com/seotlt/golang_bch/protocol/wallet"

	"gitlab.com/seotlt/golang_bch/protocol"

	"gopkg.in/abiosoft/ishell.v2"
)

func main() {
	shell := ishell.New()

	// display welcome info.
	shell.Println("Добро пожаловать в консольную утилиту для настроки генезисного блока!\n")
	shell.Println("Введите приватный ключ кошелька (оставьте пустым, если хотите создать новый):")

	var genesisWl *wallet.Wallet
	for {
		privKey := shell.ReadPassword()
		privKeyLen := len(privKey)
		if privKeyLen == 66 {
			privKey = privKey[2:]
		}

		if privKeyLen != 64 &&
			privKeyLen != 0 {
			shell.Println("Неверный приватный ключ! Попробуйте снова:")
			continue
		}

		var err error
		genesisWl, err = wallet.GenWallet(privKey)
		if err != nil {
			shell.Println("Ошибка:", err)
			shell.Println("Попробуйте снова:")
			continue
		}

		break
	}

	shell.Printf("Выбран кошелек:\nAddress: %s\nPrivateKey: %s\nPublicKey: %s\n",
		genesisWl.Address, genesisWl.PrivateKey, genesisWl.PublicKey)

	shell.Println("Введите начальное количество монет типа 'coin' [1000000000000]:")
	var coinsAmount int
	for {
		coinsAmountStr := shell.ReadLine()
		if len(coinsAmountStr) == 0 {
			coinsAmount = 1000000000000
		} else {
			var err error
			coinsAmount, err = strconv.Atoi(coinsAmountStr)
			if err != nil {
				shell.Println("Ошибка:", err)
				shell.Println("Попробуйте снова:")

				continue
			}
		}

		break
	}

	shell.Println("Введите начальное количество монет типа 'state' [1000000000]:")
	var statesAmount int
	for {
		statesAmountStr := shell.ReadLine()
		if len(statesAmountStr) == 0 {
			statesAmount = 1000000000
		} else {
			var err error
			statesAmount, err = strconv.Atoi(statesAmountStr)
			if err != nil {
				shell.Println("Ошибка:", err)
				shell.Println("Попробуйте снова:")

				continue
			}
		}

		break
	}

	genesisCoin := getGenesisCoin(*genesisWl, coinsAmount)
	genesisState := getGenesisState(*genesisWl, statesAmount)

	genesisBlock := getGenesisBlock(genesisCoin, genesisState, *genesisWl)
	genesisBlockJ, err := json.Marshal(genesisBlock)
	if err != nil {
		panic(err)
	}

	var genesisFile *os.File
	if _, err := os.Stat("./genesis.json"); os.IsExist(err) {
		genesisFile, _ = os.Open("./genesis.json")
		genesisFile.Truncate(0)
	} else {
		genesisFile, _ = os.Create("./genesis.json")
	}

	genesisFile.Write(genesisBlockJ)

	genesisFile.Close()

	shell.Println("Генезисный блок успешно создан! :)")
}

func getGenesisCoin(genesisWallet wallet.Wallet, initAmount int) protocol.TransactionInt {
	inputs := []protocol.Input{
		protocol.Input{
			TxHash: "0",
			Index:  0,
			Amount: initAmount,
		},
	}
	outputs := []protocol.Output{
		protocol.Output{
			Address: genesisWallet.Address,
			Amount:  initAmount,
		},
	}

	tx := protocol.NewCoin(
		"",
		genesisWallet.Address,
		int8(0),
		0,
		inputs,
		outputs,
		genesisWallet.PublicKey,
		0,
		"",
	)

	tx.SignTX(genesisWallet.PrivateKey)

	return tx
}

func getGenesisState(genesisWallet wallet.Wallet, initAmount int) protocol.TransactionInt {
	inputs := []protocol.Input{
		protocol.Input{
			TxHash: "0",
			Index:  0,
			Amount: initAmount,
		},
	}
	outputs := []protocol.Output{
		protocol.Output{
			Address: genesisWallet.Address,
			Amount:  initAmount,
		},
	}

	tx := protocol.NewCoin(
		"",
		genesisWallet.Address,
		int8(1),
		0,
		inputs,
		outputs,
		genesisWallet.PublicKey,
		0,
		"",
	)

	tx.SignTX(genesisWallet.PrivateKey)

	return tx
}

func getGenesisBlock(genesisCoin, genesisState protocol.TransactionInt, genesisWallet wallet.Wallet) *protocol.Block {
	transactions := []protocol.TransactionInt{
		genesisCoin,
		genesisState,
	}

	genesisBlock := protocol.NewBlock(
		0,
		"",
		"0",
		transactions,
		0,
		153722867,
		"0",
		"",
		"0",
		genesisWallet.Address,
		genesisWallet.PublicKey,
	)

	genesisBlock.Sign(genesisWallet.PrivateKey)

	return genesisBlock
}
